.PHONY: docs clean package install

package: setup.cfg
	python -m build

install: package
	pip install .

setup.cfg:
	python ci/bake_version_number.py

docs:
	make -C docs

clean:
	rm -rf dist setup.cfg src/*.iss src/*.spec src/run_*.py src/version_info_*.txt src/online_choir.egg-info
	git checkout -- src/onlinechoir/common/version.py
	make -C docs clean
