\documentclass[a4paper]{article}
\usepackage{graphicx}
\usepackage{hyperref}
\graphicspath{ {./figures/} }

\title{User Manual for Singers\\ \large \input{version_string}}

\begin{document}
    \maketitle

    \section{Installation}\label{sec:installation}

    In order to use the software, you will need headphones.
    Please make sure that they are properly connected and work nicely before you start installing the software.
    Make sure that the headphones are selected as default audio output device in your system preferences before you
    start the program.
    You can use any other app to play some audio and make sure that the headphone are working correctly.
    Please also make sure that the intended audio input device (aka microphone) is selected as default input.

    \subsection{Mac OS}\label{subsec:mac-os}

    \begin{enumerate}
        \item Download the app from\\
              \url{https://gitlab.com/odousse/online-choir/-/jobs/artifacts/master/download?job=mac-singer-app}
        \item Unzip the application by clicking on the file you just downloaded ("Online Choir Singer Mac.zip").
              This will create the application "Online Choir Singer" next to the compressed file.
              Some browsers also uncompress zip files automatically, so you might find the "Online Choir Singer"
              application already uncompressed in your "Download" folder.
              You are welcome to move the application to a more appropriate place at this point, for example the
              "Applications" folder.
        \item The first time you open the application, as it is not an "official" software (the code is not notarized
              by Apple), you will see a warning that the application originates from an unknown developer.
              In order to proceed, you will have to open the "Security and Privacy" preference panel
              (see Figure~\ref{fig:prefs}).
              There in the tab "General", you will find an "Open Anyways" button.
              This will allow you to open the application.
              This operation does not need to be repeated when you subsequently open the application.
    \end{enumerate}

    \begin{figure}[htb]
        \label{fig:prefs}
        \begin{center}
            \includegraphics[scale=0.4]{prefs.png}
        \end{center}
        \caption{The Preferences pane where you can force your Mac to open the program}
    \end{figure}

    \subsection{Windows}\label{subsec:windows}

    \begin{enumerate}
        \item Download the app from\\
              \url{https://gitlab.com/odousse/online-choir/-/jobs/artifacts/master/raw/Output/online-choir-singer-setup.exe?job=win-app}
              (installer) or \\
              \url{https://gitlab.com/odousse/online-choir/-/jobs/artifacts/master/raw/dist/Online\%20Choir\%20Singer.exe?job=win-app}
              (stand-alone app).
        \item Launch the installer or app.
              As the application is not "official" (i.e. it is not coming with a cryptographic signature certifying its
              provenance), you might see a warning that the application is from an unknown developer.
              Please override and proceed.
    \end{enumerate}

    \subsection{Linux (Debian/Ubuntu)}\label{subsec:linux}

    \begin{enumerate}
        \item Install the following packages: \texttt{python3-tk, python3-pip, python3-numpy,
              python3-soundfile, libportaudio2} and \texttt{libsndfile1}.
              This can be done using the terminal and issue the following command:\\
              \texttt{sudo apt install python3-tk python3-pip python3-numpy python3-soundfile libportaudio2 libsndfile1}
        \item Install the program itself:\\
              \texttt{pip3 install online-choir}
        \item Finally, in order to start the program, type \texttt{online-choir-singer} and press enter.
    \end{enumerate}
    So many steps are needed the first time.
    The next time you want to open the program, you only need to repeat steps 4, 5 and 7.

    \section{Audio setup}\label{sec:audio-setup}

    If you haven't opened the program yet, open it now.
    You should see a small window, indicating "READY" as shown on Figure~\ref{fig:ready}.
    If you use the program for the first time, you will be reminded that you need to calibrate your system
    (see Figure\ref{fig:need_calib}).
    We will show you how to do this in Section~\ref{subsec:claib}; we will come to it, please don't jump the steps
    before.

    In order to set up your audio system, please open the settings window by clicking on the "Settings" button.
    The settings window is depicted in Figure~\ref{fig:settings}.

    MAC USERS: on modern versions of Mac OS, at this point, you might be prompted with a request to allow the program to
    record from your microphone (see Figure~\ref{fig:prompt}).
    Please agree and proceed.

    \begin{figure}[htb]
        \label{fig:ready}
        \begin{center}
            \includegraphics[scale=0.5]{ready.png}
        \end{center}
        \caption{The main program window when it is ready}
    \end{figure}

    \begin{figure}[htb]
        \label{fig:need_calib}
        \begin{center}
            \includegraphics[scale=0.5]{calib.png}
        \end{center}
        \caption{The main program window when calibration is needed}
    \end{figure}

    \begin{figure}[htb]
        \label{fig:settings}
        \begin{center}
            \includegraphics[scale=0.5]{settings.png}
        \end{center}
        \caption{The audio settings window}
    \end{figure}

    \begin{figure}[htb]
        \label{fig:prompt}
        \begin{center}
            \includegraphics[scale=0.5]{prompt.png}
        \end{center}
        \caption{Dialog on Mac asking for permission to use the microphone}
    \end{figure}

    \subsection{Input level}\label{subsec:input}

    At the top of the settings window, you can watch the input level of the microphone.
    Please sing a bit to give it a try.
    You should see the level bar moving.
    The microphone sensitivity can be adjusted wit the scale just below the level bar.
    In order to get heard properly, the level when you sing should be in the green or orange area.
    It can even get orange at times, that's not a problem.
    If it gets red, you have to decrease the microphone sensitivity.
    If it is not reaching the green zone, you have to increase the sensitivity.

    The important parts here are that:
    \begin{itemize}
        \item You can see the level moving when you sing
        \item The level is at least green and not red
    \end{itemize}
    In this way, you ensure that your voice fits nicely into the mix.

    \subsection{Audio Output}\label{subsec:output}

    With the "Play Sample Sound" button, you can check that the program can play sounds into you headphones.
    Please click it and make sure that you hear the sound sample loud and clear.
    Please adjust the system volume as needed.

    \subsection{Calibration}\label{subsec:claib}

    In order to mix all voices in the choir in a synchronous way, the latency of your audio hardware needs to be
    measured, and the program configured to compensate for this latency.
    This is what the Calibration step achieves.

    In order to perform the calibration, you need to follow these steps:
    \begin{enumerate}
        \item Place your headphones close to the microphone.
              \uppercase{Do not keep the headphones on your hears}!
              If you use your laptop's integrated microphone, you will have to investigate where it is hidden.
              Please search for on Internet for the answer.
        \item Click on the "Calibrate" button.
              This will play an unpleasant sound in your headphones, but luckily they are no longer on your ears.
        \item After a few seconds, a latency value is computed and shown.
              Please verify that the message "calibration failed" is not shown instead.
        \item If you see "calibration failed", try to increase the volume of the headphones and/or bring the headphones
              closer to the microphone
        \item Repeat the operation a couple of times, in order to check that a similar value is measured each time.
              If the number does not differ by more than one unit between attempts, it means that the calibration works
              well.
    \end{enumerate}

    After calibration is complete, you can close the Settings window.
    You should now see the "READY" message as shown in Figure~\ref{fig:ready}.
    You are now ready to sing!
    
    \subsubsection{Manual latency setting}
    
    Optionally, you can adjust the latency value manually. 
    \emph{However, we strongly discourage you to do it without checking afterwards if your voice is better synchronized with the altered value.} 
    Finding the correct latency value manually can be a very tedious task.

    \section{Singing}\label{sec:sing}

    There is nothing else you need to do.
    Everything else is controlled by your director.
    As long as your program shows "READY", you are good to go.

    If the rehearsal is not about to start, you can close the program.
    The calibration will be saved and reloaded automatically when you reopen the program.

\end{document}
