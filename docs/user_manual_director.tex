\documentclass[a4paper]{article}
\usepackage{graphicx}
\usepackage{hyperref}
\graphicspath{ {./figures/} }

\title{User Manual for Directors\\ \large \input{version_string}}

\begin{document}
    \maketitle

    \section{Generalities}\label{sec:generalities}

    This software handles audio only and is meant to be used typically in conjunction with a video-conferencing
    software.
    However, it does not depend on any other software can technically be used alone.

    The software has three components:

    \begin{itemize}
        \item The software for singers: the software that participants need to install on their computer.
              It is the subject of separate instructions ("user\_manual\_singer") that need to be distributed to
              participants.
        \item The software for directors: this piece is described in this document.
        \item The server software: in order to use the software, a server is also needed.
              Please make sure that you have a server instance running and available for your choir.
              In the sequel, we assume that you have a server instance ready, for which you know the following
              (see also Section~\ref{sub:server}):
              \begin{itemize}
                  \item Address (usually its DNS name, or alternatively an IP address)
                  \item Master port number
                  \item Slave port number
              \end{itemize}
              If you do not have such a server, you need to create one.
              This is the subject of separate instructions (to be added).
    \end{itemize}

    \section{Installation}\label{sec:installation}

    \subsection{Mac OS}\label{subsec:mac-os}

    \begin{enumerate}
        \item Download the app from\\
              \url{https://gitlab.com/odousse/online-choir/-/jobs/artifacts/master/download?job=mac-director-app}
              As a director, you should select "Online Choir Director vX.X".
              Please select the latest version X.X available, unless you have good reasons not to do so.
        \item Unzip the application by clicking on the file you just downloaded ("Online Choir Director Mac vX.X.zip").
              This will create the application "Online Choir Singer" next to the compressed file.
              Some browsers also uncompress zip files automatically, so you might find the "Online Choir Director"
              application already uncompressed in your "Download" folder.
              You are welcome to move the application to a more appropriate place at this point, for example the
              "Applications" folder.
        \item The first time you open the application, as it is not an "official" software (the code is not notarized
              by Apple), you will see a warning that the application originates from an unknown developer.
              In order to proceed, you will have to open the "Security and Privacy" preference panel
              (see Figure~\ref{fig:prefs}).
              There in the tab "General", you will find an "Open Anyways" button.
              This will allow you to open the application.
              This operation does not need to be repeated when you subsequently open the application.
    \end{enumerate}

    \begin{figure}[htb]
        \label{fig:prefs}
        \begin{center}
            \includegraphics[scale=0.4]{prefs.png}
        \end{center}
        \caption{The Preferences pane where you can force your Mac to open the program}
    \end{figure}

    \subsection{Windows}\label{subsec:windows}

    \begin{enumerate}
        \item Download the app from\\
              \url{https://gitlab.com/odousse/online-choir/-/jobs/artifacts/master/raw/src/Output/online-choir-director-setup.exe?job=win-app}
              (installer) or \\
              \url{https://gitlab.com/odousse/online-choir/-/jobs/artifacts/master/raw/src/dist/Online\%20Choir\%20Director.exe?job=win-app}
              (stand-alone app).
        \item Launch the installer or app.
              As the application is not "official" (i.e. it is not coming with a cryptographic signature certifying its
              provenance), you might see a warning that the application is from an unknown developer.
              Please override and proceed.
    \end{enumerate}

    \section{Interface}\label{sec:interface}

    The main program window is shown on Figure~\ref{fig:main}.

    \begin{figure}[htb]
        \label{fig:main}
        \begin{center}
            \includegraphics[scale=0.5]{director.png}
        \end{center}
        \caption{Main window}
    \end{figure}

    \subsection{Lead track selection}\label{sub:lead}

    In order to sing, the choir needs an audio cue.
    The director needs to prepare an audio track (the lead track) along which the choir is to sing.
    The track \emph{must} be in the following format:
    \begin{itemize}
        \item WAV format
        \item Sampling rate 44100Hz
        \item 16 bits per sample
        \item Ideally 2 channels (stereo), although 1 channel (mono) is also supported
    \end{itemize}

    The lead track can be selected using the "Open" button.
    Once successfully opened, the following controls are activated:
    \begin{itemize}
        \item Start point scale: This scale allows you to choose a position in the track from which you want to start
              playing.
              If you just want to play from the start of the track, leave the scale's cursor at the left end.
        \item Play locally: This allows to play the track on your computer.
              It is useful to verify that the track is the correct one, or to adjust the start point using the scale
              above.
        \item Play to everyone: This is the button that activates the main function of this software.
              See Section~\ref{sub:session}.
    \end{itemize}

    \subsection{Track-based session}\label{sub:session}

    Before starting the singing session itself, IF YOU ARE USING THE SOFTWARE ALONG WITH A VIDEO-CONFERENCING SOFTWARE,
    MAKE SURE OF THE FOLLOWING:
    \begin{itemize}
        \item All participants, \emph{including yourself}, are on mute in the video-conferencing software.
              Usually, such software offers a "mute everyone" feature that eases this step.
        \item You are \emph{not} sharing any audio through that software ("share my computer audio" or similar features)
    \end{itemize}
    FAILURE TO FOLLOW THESE STEPS WILL RESULT IN AWFUL ECHO.

    Once you have selected the lead track (and optionally a start point in this track), press the "Play to Everyone"
    button to start the session.
    It will start playing the track on every participant's computer, for them to sing along.

    After 4-5 seconds, you will start hearing the choir, mixed together with the track you sent.
    You can adjust the mix between choir and lead track using the field below the "Save Recording" button.
    The value in the field is the relative loudness of the track in dBs.
    For instance, a value of -10 indicates that the track's level is 10 dBs lower than that of the choir\footnote{
        Note that some of the lead track usually leaks into the mix through the singers' computers audio feedback, too.
        This can note be controlled or removed.
        The only way to reduce this is to urge the singers to use headphones.
    }.

    The session will end when the end the track ends or when you press "Stop".

    \subsection{Live session}\label{sub:live}

    Alternatively to a lead track, you can also stream audio from your computer directly to choir to sing along.
    To do this, press the "Live Session" button.
    IF YOU ARE USING THE SOFTWARE ALONG WITH A VIDEO-CONFERENCING SOFTWARE, THE SAME PRECAUTIONS APPLY AS FOR
    TRACK-BASED SESSIONS (see Section~\ref{sub:session}).
    The session goes on until you press "Stop".

    In this mode though, you can not hear the choir as you play.
    Instead, you have to use the recording function (see Section~\ref{sub:rec})in order to hear the result afterwards.

    MAC USERS: on modern versions of Mac OS, at this point, you might be prompted with a request to allow the program to
    record from your microphone (see Figure~\ref{fig:prompt}).
    Please agree and proceed.

    \begin{figure}[htb]
        \label{fig:prompt}
        \begin{center}
            \includegraphics[scale=0.5]{prompt.png}
        \end{center}
        \caption{Dialog on Mac asking for permission to use the microphone}
    \end{figure}

    \subsection{Recording}\label{sub:rec}

    During each a session (either using a lead track or live), the choir is being recorded.
    You can listen to the recording after each session using the "Play Recording" button
    This will play the recording on your computer only\footnote{
        If you wish to let the choir hear the recording, you have to rely on your video-conferencing software to share
        your computer audio with the choir while playing the recording. REMEMBER TO NOT SHARE YOUR COMPUTER AUDIO
        DURING THE SESSION BUT ONLY AFTER.
    }.
    You can also save the recording into a WAV file by pressing the "Save Recording" button.
    
    \subsection{Mixing desk}\label{sub:desk}
    
    During sessions, you can open the mixing desk using the corresponding button. 
    The mixing desk allows to change the volume of each singer.
    The first channel of the desk ("Monitor") controls the volume of the original lead track in the mix.
    However, this channel is excluded from the recording.
    Since the mix is not audible during live sessions, this channel is not useful in such sessions.
    
    There are a few limitations to the mixing desk feature that are important to keep in mind:
    \begin{itemize}
    	\item The changes are audible in the mix with a long delay (a couple of seconds).
    	      Please beware of that when trying to adjust the volumes.
    	\item The changes you make to the mix are saved locally on your computer, so you can reload them in a future session.
    	      However, the reloading of previous changes only happens if you open the mixing desk.
    \end{itemize}

    \subsection{Audio interface selection}\label{sub:interfaces}

    If needed, you can select the audio interface used by the software to play audio on your computer (output device),
    and to pick up audio for live sessions (input device).
    It is recommended to chose "System Default" for both, which will cause the software to use your usual input/output
    devices.
    If the program uses the wrong device (e.g.~you are wearing headphones, but the program keeps playing on your
    speakers), you can select the device manually here.

    Note that the list of available devices is initialized when the program starts.
    If this list changes later (e.g.~you connect some external audio interface after starting the program), you have to
    quit and restart the program.

    \subsection{Server selection}\label{sub:server}

    The program uses "online-choir.modetexte.ch", port 8878 by default.
    This server usually works fine, but is shared with all users of the program.
    You can use this server for testing, but other choirs may be using it at the same time, which will potentially
    trigger funny situations.

    It is recommended that you secure your own server instance.
    As you are running the director piece, you need to use the \emph{master port number}.
    Make sure that your participants use the same server address and the corresponding slave port number.

\end{document}
