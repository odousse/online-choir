import subprocess

try:
    res = subprocess.run(["git", "describe", "--tags"], check=True, capture_output=True)
    VERSION = res.stdout.decode().strip()
    if VERSION[0] == 'v':  # strip initial 'v'
        VERSION = VERSION[1:]
except (subprocess.CalledProcessError, FileNotFoundError, ValueError, IndexError):
    VERSION = "UNKNOWN_VERSION"

