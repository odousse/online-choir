import sys
from pathlib import Path
from string import Template

project_root = str(Path(__file__).parent.resolve().parent / "src")
if project_root not in sys.path:
    sys.path.append(project_root)

from onlinechoir.common.version import VERSION


def convert_version_to_numbers(version: str):

    # strip second part
    version = version.split("-")[0]

    # extract numbers
    res = []
    for n in version.split("."):
        try:
            i = int(''.join(c for c in n if c.isdigit()))
        except ValueError:
            continue
        res.append(i)
    return res


def main():
    version_numbers = convert_version_to_numbers(VERSION)
    for _ in range(4 - len(version_numbers)):  # pad with zeros
        version_numbers.append(0)
    file_version = ", ".join(str(i) for i in version_numbers)

    print(f"version = {VERSION}")
    print(f"file_version = {file_version}")

    def create_file_from_template(template_path: Path, dest_path: Path):
        t = Template(template_path.read_text())
        with (dest_path / template_path.stem).open("w") as f:
            f.write(t.substitute(version=VERSION, file_version=file_version))

    template_dir = Path("assets/templates")
    create_file_from_template(template_dir / "version.py.template", Path("src/onlinechoir/common"))
    create_file_from_template(template_dir / "online-choir-director.iss.template", Path("src"))
    create_file_from_template(template_dir / "online-choir-singer.iss.template", Path("src"))
    create_file_from_template(template_dir / "pyi_director_bundle.spec.template", Path("src"))
    create_file_from_template(template_dir / "pyi_director_one_file.spec.template", Path("src"))
    create_file_from_template(template_dir / "pyi_singer_bundle.spec.template", Path("src"))
    create_file_from_template(template_dir / "pyi_singer_one_file.spec.template", Path("src"))
    create_file_from_template(template_dir / "version_info_director.txt.template", Path("src"))
    create_file_from_template(template_dir / "version_info_singer.txt.template", Path("src"))
    create_file_from_template(template_dir / "run_master_app.py.template", Path("src"))
    create_file_from_template(template_dir / "run_slave_app.py.template", Path("src"))
    create_file_from_template(template_dir / "setup.cfg.template", Path("."))
    create_file_from_template(template_dir / "version_string.tex.template", Path("docs"))


if __name__ == '__main__':
    main()
