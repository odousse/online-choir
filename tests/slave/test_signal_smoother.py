from hamcrest import assert_that, equal_to, is_

from onlinechoir.slave.level_meter import DotLevelMeter
from onlinechoir.slave.signal_smoother import SignalSmoother


def test_higher_always_pass():
    smoother = SignalSmoother(0, 3)
    for l in range(1, 5):
        assert_that(smoother(l), equal_to(l))


def test_lower_takes_longer():
    smoother = SignalSmoother(0, 3)
    assert_that(smoother(5), equal_to(5))
    assert_that(smoother(-1), is_(None))
    assert_that(smoother(-3), is_(None))
    assert_that(smoother(-2), is_(None))
    assert_that(smoother(-1), equal_to(-1))


def test_reset_age():
    smoother = SignalSmoother(0, 3)
    assert_that(smoother(0), is_(None))
    assert_that(smoother(-1), is_(None))
    assert_that(smoother(-3), is_(None))
    assert_that(smoother(0), is_(None))  # this resets the age of the 0 value
    assert_that(smoother(-1), is_(None))
    assert_that(smoother(-3), is_(None))
    assert_that(smoother(-2), is_(None))
    assert_that(smoother(-1), equal_to(-1))


def test_with_derivative_class():
    smoother = SignalSmoother(0, 3)
    assert_that(smoother(DotLevelMeter.SignalLevel.red), is_(DotLevelMeter.SignalLevel.red))
    assert_that(smoother(DotLevelMeter.SignalLevel.orange), is_(None))
    assert_that(smoother(DotLevelMeter.SignalLevel.green), is_(None))
    assert_that(smoother(DotLevelMeter.SignalLevel.red), is_(None))  # this resets the age of the 'red' value
    assert_that(smoother(DotLevelMeter.SignalLevel.green), is_(None))
    assert_that(smoother(DotLevelMeter.SignalLevel.green), is_(None))
    assert_that(smoother(DotLevelMeter.SignalLevel.orange), is_(None))
    assert_that(smoother(DotLevelMeter.SignalLevel.orange), is_(DotLevelMeter.SignalLevel.orange))


def test_no_sharp_drop():
    smoother = SignalSmoother(0, 3)
    assert_that(smoother(0), is_(None))
    assert_that(smoother(-1), is_(None))
    assert_that(smoother(-3), is_(None))
    assert_that(smoother(-2), is_(None))
    assert_that(smoother(-3), equal_to(-1))
    assert_that(smoother(-3), equal_to(-2))


def test_reset():
    smoother = SignalSmoother(0, 3)
    assert_that(smoother(5), equal_to(5))
    smoother.reset()
    assert_that(smoother(0), is_(None))
    assert_that(smoother(3), equal_to(3))
    assert_that(smoother(2), is_(None))
    assert_that(smoother(2), is_(None))
    assert_that(smoother(2), is_(None))
    assert_that(smoother(2), equal_to(2))
