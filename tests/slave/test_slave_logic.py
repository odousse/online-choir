import asyncio
from hamcrest import assert_that, equal_to

from onlinechoir.common.audio_codecs import StereoEncoder
from onlinechoir.common.constants import CONNECTION_TIMEOUT, KEEPALIVE_INTERVAL
from onlinechoir.common.log import setup_logging
from onlinechoir.slave.gui_enums import GuiState, GuiEvent
from onlinechoir.slave.slave_client import SlaveClient
from ..utils import MockServer, GuiTracker, wait_until, SlaveTestCase

setup_logging('DEBUG', None)


class TestSlaveLogic(SlaveTestCase):
    async def test_normal_session(self):
        gui_tracker = GuiTracker()
        async with MockServer() as test_server:
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            test_server.send_chunk(StereoEncoder.silence_frame(0))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)

            # send audio
            for idx in range(1, 100):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))
            test_server.send_chunk(StereoEncoder.silence_frame(-1))

            await wait_until(lambda: client._audio._input_queue.qsize() >= 111)
            await self.stream.tick_all()
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

            # check result
            test_server.assert_received_sessions_are([(100, False)], 1)

    async def test_multiple_sessions(self):
        gui_tracker = GuiTracker()
        async with MockServer() as test_server:
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # first session
            for idx in range(100):
                test_server.out_queue.put_nowait(StereoEncoder.silence_frame(idx))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)
            test_server.out_queue.put_nowait(StereoEncoder.silence_frame(-1))
            await wait_until(lambda: client._audio._input_queue.qsize() >= 111)
            await self.stream.tick_all()
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # second session
            for idx in range(100):
                test_server.out_queue.put_nowait(StereoEncoder.silence_frame(idx))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)
            test_server.out_queue.put_nowait(StereoEncoder.silence_frame(-1))
            await wait_until(lambda: client._audio._input_queue.qsize() >= 111)
            await self.stream.tick_all(2)
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

            # check result
            test_server.assert_received_sessions_are([(100, False), (100, False)], 2)

    async def test_settings(self):
        gui_tracker = GuiTracker()
        async with MockServer():
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # press button
            gui_tracker.events_out.put_nowait(GuiEvent.SETTINGS_BUTTON.value)
            await gui_tracker.wait_that_state_is(GuiState.SETTINGS)
            gui_tracker.close_settings()
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

    async def test_underflow(self):
        gui_tracker = GuiTracker()
        async with MockServer() as test_server:
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            test_server.send_chunk(StereoEncoder.silence_frame(0))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)

            # send audio
            for idx in range(1, 100):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))

            await wait_until(lambda: client._audio._input_queue.qsize() >= 100)
            await wait_until(lambda: self.stream.n_instances == 1)
            for i in range(100):
                self.stream.tick()

            self.stream.tick()  # one tick too far
            assert_that(client._audio._failed)
            await asyncio.sleep(KEEPALIVE_INTERVAL + 0.1)  # trigger a keep-alive

            # some more audio
            for idx in range(100, 150):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))
            test_server.send_chunk(StereoEncoder.silence_frame(-1))

            await wait_until(lambda: client._audio._input_queue.qsize() >= 61)
            await self.stream.tick_all()
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # second session
            for idx in range(100):
                test_server.out_queue.put_nowait(StereoEncoder.silence_frame(idx))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)
            test_server.out_queue.put_nowait(StereoEncoder.silence_frame(-1))
            await wait_until(lambda: client._audio._input_queue.qsize() >= 111)
            await self.stream.tick_all(2)
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

            # check result
            test_server.assert_received_sessions_are([(90, True), (100, False)], 3)

    async def test_late_join(self):
        gui_tracker = GuiTracker()
        async with MockServer() as test_server:
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            for idx in range(10, 100):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))
            test_server.out_queue.put_nowait(StereoEncoder.silence_frame(-1))
            assert_that(gui_tracker.state, equal_to(GuiState.READY))

            # second session
            for idx in range(100):
                test_server.out_queue.put_nowait(StereoEncoder.silence_frame(idx))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)
            test_server.out_queue.put_nowait(StereoEncoder.silence_frame(-1))
            await wait_until(lambda: client._audio._input_queue.qsize() >= 111)
            await self.stream.tick_all()
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

            # check result
            test_server.assert_received_sessions_are([(100, False)], 1)

    async def test_short_session(self):
        gui_tracker = GuiTracker()
        async with MockServer() as test_server:
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            test_server.send_chunk(StereoEncoder.silence_frame(0))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)

            # send audio
            for idx in range(1, 50):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))
            test_server.send_chunk(StereoEncoder.silence_frame(-1))

            await wait_until(lambda: client._audio._input_queue.qsize() >= 61)
            await self.stream.tick_all()
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

            # check result
            test_server.assert_received_sessions_are([(50, False)], 1)

    async def test_settings_open_when_session_starts(self):
        gui_tracker = GuiTracker(settings_busy=True)
        async with MockServer() as test_server:
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # press button
            gui_tracker.events_out.put_nowait(GuiEvent.SETTINGS_BUTTON.value)
            await gui_tracker.wait_that_state_is(GuiState.SETTINGS)

            # start session
            for idx in range(100):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))
            await asyncio.sleep(0.5)  # wait a bit...

            # close settings
            gui_tracker.close_settings(force=True)
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # finish session
            for idx in range(100, 200):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))
            test_server.send_chunk(StereoEncoder.silence_frame(-1))

            await asyncio.sleep(0.5)  # wait a bit...
            assert_that(gui_tracker.state, equal_to(GuiState.READY))  # nothing happens
            assert_that(client._audio._input_queue.empty())  # all audio skipped

            # second session
            for idx in range(100):
                test_server.out_queue.put_nowait(StereoEncoder.silence_frame(idx))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)
            test_server.out_queue.put_nowait(StereoEncoder.silence_frame(-1))
            await wait_until(lambda: client._audio._input_queue.qsize() >= 111)
            await self.stream.tick_all()
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

            # check result
            test_server.assert_received_sessions_are([(100, False)], 3)

    async def test_auto_close_settings(self):
        gui_tracker = GuiTracker()
        async with MockServer() as test_server:
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # press button
            gui_tracker.events_out.put_nowait(GuiEvent.SETTINGS_BUTTON.value)
            await gui_tracker.wait_that_state_is(GuiState.SETTINGS)

            test_server.send_chunk(StereoEncoder.silence_frame(0))
            await gui_tracker.wait_that_state_is(GuiState.READY)  # auto-close settings
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)  # normal session

            # send audio
            for idx in range(1, 100):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))
            test_server.send_chunk(StereoEncoder.silence_frame(-1))

            await wait_until(lambda: client._audio._input_queue.qsize() >= 111)
            await self.stream.tick_all()
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

            # check result
            test_server.assert_received_sessions_are([(100, False)], 3)

    async def test_connection_timeout(self):
        gui_tracker = GuiTracker()
        async with MockServer():
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            await gui_tracker.wait_that_state_is(GuiState.DISCONNECTED, CONNECTION_TIMEOUT + KEEPALIVE_INTERVAL + 1)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

    async def test_connection_closed(self):
        gui_tracker = GuiTracker()
        async with MockServer():
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

        await gui_tracker.wait_that_state_is(GuiState.DISCONNECTED, KEEPALIVE_INTERVAL + 1)

        # exit client
        gui_tracker.events_out.put_nowait(None)
        await asyncio.wait_for(client_logic, 1)

    async def test_connection_closed_during_session(self):
        gui_tracker = GuiTracker()
        async with MockServer() as test_server:
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            test_server.send_chunk(StereoEncoder.silence_frame(0))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)

            # send audio
            for idx in range(1, 100):
                test_server.send_chunk(StereoEncoder.silence_frame(idx))
            await wait_until(lambda: client._audio._input_queue.qsize() >= 100)
            await wait_until(lambda: self.stream.n_instances == 1)
            for i in range(50):
                self.stream.tick()  # tick half way

        # disconnect
        await wait_until(lambda: client._audio._input_queue.qsize() >= 51)
        for i in range(51):
            self.stream.tick()  # tick the other half
        await gui_tracker.wait_that_state_is(GuiState.DISCONNECTED)

        # exit client
        gui_tracker.events_out.put_nowait(None)
        await asyncio.wait_for(client_logic, 1)

    async def test_start_disconnected(self):
        gui_tracker = GuiTracker()
        config = {'server': 'localhost', 'port': 8867, 'latency': 10}
        async with MockServer():
            client = SlaveClient(config, gui_tracker, True)
            client_logic = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.DISCONNECTED)

            # press button
            gui_tracker.events_out.put_nowait(GuiEvent.SETTINGS_BUTTON.value)
            await gui_tracker.wait_that_state_is(GuiState.SETTINGS)
            config['port'] = 8868  # fix port number
            gui_tracker.close_settings()
            await gui_tracker.wait_that_state_is(GuiState.CONNECTING)
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)

    async def test_try_reconnect(self):
        gui_tracker = GuiTracker()
        config = {'server': 'localhost', 'port': 8868, 'latency': 10}

        client = SlaveClient(config, gui_tracker, True)
        client_logic = asyncio.create_task(client.main())
        await gui_tracker.wait_that_state_is(GuiState.DISCONNECTED)

        # press button
        gui_tracker.events_out.put_nowait(GuiEvent.SETTINGS_BUTTON.value)
        await gui_tracker.wait_that_state_is(GuiState.SETTINGS)

        async with MockServer():  # start server
            gui_tracker.close_settings()
            await gui_tracker.wait_that_state_is(GuiState.CONNECTING)
            await gui_tracker.wait_that_state_is(GuiState.READY)

            # exit client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(client_logic, 1)
