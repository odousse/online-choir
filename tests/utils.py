import asyncio
import numpy as np
from aiounittest import AsyncTestCase
from collections import deque, defaultdict
from dataclasses import dataclass
from functools import partial
from hamcrest import assert_that, equal_to, greater_than_or_equal_to
from sounddevice import CallbackStop, CallbackAbort
from typing import Iterable, Tuple, Optional, Callable, List
from unittest import mock

from onlinechoir.common.audio_codecs import AdpcmDecoder, StereoDecoder, AudioEncoder
from onlinechoir.common.constants import FRAME_SIZE, FrameType
from onlinechoir.common.log import logger
from onlinechoir.common.auth import HEADER_SIZE
from onlinechoir.common.utils import iterate_queue, iterate_stream
from onlinechoir.slave.gui_enums import GuiState, GuiEvent


async def wait_until(condition: Callable[[], bool], timeout: Optional[float] = 1.):
    t = 0.
    while not condition():
        await asyncio.sleep(0.1)
        t += 0.1
        if t >= timeout:
            raise asyncio.TimeoutError("Condition not met")
    return


def silence_chunk():
    return np.zeros((FRAME_SIZE, 2))


class MockServer:
    def __init__(self):
        self.task = None
        self.in_queue = asyncio.Queue()
        self.out_queue = asyncio.Queue()
        self._server_ready = asyncio.Event()
        self.sub_task = None

    async def __aenter__(self):
        self.task = asyncio.create_task(self.loop())
        await asyncio.wait_for(self._server_ready.wait(), 1)
        return self

    async def loop(self):
        try:
            server = await asyncio.start_server(self.create_serve_task, port=8868)
            async with server:
                self._server_ready.set()
                await server.serve_forever()
        except asyncio.CancelledError:
            logger.info(" * Server terminated")

    async def serve(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        await reader.readexactly(HEADER_SIZE)  # discard header
        sender = asyncio.create_task(self.send(writer))
        try:
            await self.receive(reader)
        except asyncio.CancelledError:
            logger.info(" * Server task terminated")
        self.out_queue.put_nowait(None)
        try:
            await sender
        except asyncio.CancelledError:
            pass

    async def create_serve_task(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        if self.sub_task is not None:
            logger.error(" * Second client connected")
            writer.close()
            return
        self.sub_task = asyncio.create_task(self.serve(reader, writer))

    async def send(self, writer: asyncio.StreamWriter):
        async for item in iterate_queue(self.out_queue):
            writer.write(item)
            await writer.drain()
        logger.info(" * closing connection")
        writer.close()
        await writer.wait_closed()

    async def receive(self, reader: asyncio.StreamReader):
        async for item in iterate_stream(reader, AdpcmDecoder.PACKET_SIZE):
            self.in_queue.put_nowait(item)
        self.in_queue.put_nowait(None)
        logger.info(" * connection closed")

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self.task.cancel()
        if self.sub_task is not None:
            self.sub_task.cancel()
            await self.sub_task

    def send_chunk(self, chunk):
        assert len(chunk) == StereoDecoder.PACKET_SIZE
        self.out_queue.put_nowait(chunk)

    def assert_received_sessions_are(self, expected: Iterable[Tuple[int, bool]], keep_alives=0):
        session_iter = iter(expected)
        cur_count, cur_flag = None, None
        while not self.in_queue.empty():
            item = self.in_queue.get_nowait()
            if item is None:
                break
            idx = AdpcmDecoder.peek_frame_index(item)
            if idx == FrameType.KEEPALIVE:
                keep_alives -= 1
                continue
            if idx == FrameType.CHECK_IN:
                if cur_count is not None:
                    assert_that(cur_count, equal_to(0))
                    assert_that(not cur_flag)
                cur_count, cur_flag = next(session_iter)
            else:
                assert cur_count is not None
                if idx == FrameType.ABORT:
                    assert_that(cur_count, equal_to(0))
                    assert_that(cur_flag)
                    cur_count, cur_flag = None, None
                else:
                    assert_that(idx, greater_than_or_equal_to(0))
                    cur_count -= 1
        if cur_count is not None:
            assert_that(cur_count, equal_to(0))
            assert_that(not cur_flag)
        assert_that(keep_alives, equal_to(0))


class MockEchoServer:
    def __init__(self, buffer_size: int):
        self._buffer = asyncio.Queue(maxsize=buffer_size)
        self._ticks = asyncio.Queue()
        self._server_ready = asyncio.Event()
        self._task = None
        self.master_present = False
        self.count = 0
        self.session_complete = False

    async def __aenter__(self):
        self._task = asyncio.create_task(self.loop())
        await self._server_ready.wait()
        return self

    async def loop(self):
        server = await asyncio.start_server(self.serve, port=8878)
        async with server:
            self._server_ready.set()
            await server.serve_forever()

    async def serve(self, reader, writer):
        assert not self.master_present
        await reader.readexactly(HEADER_SIZE)  # discard header
        self.master_present = True
        decoder = StereoDecoder()
        encoder = AudioEncoder()
        sig = np.empty((FRAME_SIZE, 2), dtype=np.float32)

        async def send_item(item):
            decoder(item, sig)
            writer.write(encoder(sig.mean(axis=1)))
            await writer.drain()
            self.count += 1

        async def send():
            async for _ in iterate_queue(self._ticks):
                item = await self._buffer.get()
                if item is None:
                    break
                await send_item(item)

        async def receive():
            async for item in iterate_stream(reader, StereoDecoder.PACKET_SIZE):
                idx = StereoDecoder.peek_frame_index(item)
                if idx == -1:
                    self.session_complete = True
                else:
                    assert_that(idx >= 0)
                    if self._buffer.full():
                        await send_item(self._buffer.get_nowait())
                    self._buffer.put_nowait(item)
            logger.info(" * connection closed")

        sender = asyncio.create_task(send())
        await receive()
        sender.cancel()
        self.master_present = False

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self._task.cancel()

    def pass_frames(self, n: int):
        for _ in range(n):
            self._ticks.put_nowait(1)


@dataclass
class MockStatus:
    output_underflow: bool
    output_overflow: bool
    input_overflow: bool


class MockStream:
    def __init__(self, callback, finished_callback, latency):
        self.callback = callback
        self.finished_callback = finished_callback
        self.latency = latency
        self._delay = deque()
        for _ in range(latency):
            self._delay.append(np.zeros((FRAME_SIZE, 1)))
        self.status = 0
        self.ticks = 0

    def __enter__(self):
        assert self.status == 0
        logger.info(" * start stream")
        self.status = 1

    def __exit__(self, exc_type, exc_val, exc_tb):
        assert 1 <= self.status <= 2
        logger.info(" * stop stream")
        self.status = 3

    def tick(self):
        """Pretend to have played one frame"""
        if self.status == 0:
            logger.info(" * Tick while not started")
            return
        if self.status == 2:
            logger.info(" * Tick while already completed")
            return
        if self.status == 3:
            logger.info(" * Tick while already closed")
            return
        self.ticks += 1
        status = MockStatus(False, False, False)
        indata = self._delay.pop()
        outdata = np.empty((FRAME_SIZE, 2))
        try:
            self.callback(indata, outdata, FRAME_SIZE, None, status)
            self._delay.appendleft(outdata[:, :1])
        except (CallbackAbort, CallbackStop):
            self.status = 2
            self.finished_callback()

    async def tick_all(self):
        await wait_until(lambda: self.status == 1)
        while self.status == 1:
            self.tick()

    def set_latency(self, latency: int):
        assert self.ticks == 0
        self.latency = latency
        self._delay.clear()
        for _ in range(latency):
            self._delay.append(np.zeros((FRAME_SIZE, 1)))


class MockSounddeviceStream:
    def __init__(self, latency=10, latency_increment=0):
        logger.info(f" * new stream generator")
        self.instances: defaultdict[asyncio.Task, List[MockStream]] = defaultdict(list)
        self.latency = latency
        self.increment = latency_increment

    def reset(self):
        logger.info(f" * reset stream generator ({self.n_instances} instances used)")
        remaining_instances = self.instances
        self.instances = defaultdict(list)
        for instances in remaining_instances.values():
            for instance in instances:
                assert_that(instance.status, equal_to(3))

    def __call__(self, callback, finished_callback, **kwargs):
        task = asyncio.current_task()
        logger.info(f" * new stream with params {kwargs} from task {task}")
        stream = MockStream(callback, finished_callback, self.latency)
        self.instances[task].append(stream)
        self.latency += self.increment
        return stream

    @property
    def n_instances(self) -> int:
        return sum(len(instances) for instances in self.instances.values())

    async def set_latency(self, task: asyncio.Task, latency: int, n_instances=1):
        if n_instances:
            await wait_until(lambda: len(self.instances[task]) == n_instances)
        self.instances[task][-1].set_latency(latency)

    def tick(self):
        for instances in self.instances.values():
            for instance in instances:
                if instance.status < 2:
                    instance.tick()

    async def tick_all(self, n_instances=1):
        if n_instances:
            try:
                await wait_until(lambda: self.n_instances == n_instances)
            except asyncio.TimeoutError:
                logger.error(f"n_instances = {self.n_instances}")
                raise
        for instances in self.instances.values():
            for instance in instances:
                if instance.status < 2:
                    await instance.tick_all()


class MockIntVar:
    def __init__(self, v):
        self.v = v

    def get(self):
        return self.v


class GuiTracker:
    def __init__(self, name: Optional[str] = None, settings_busy=False):
        self.name = name
        self.events_out = None
        self.loop = None
        self.state = None
        self.changed = asyncio.Queue()
        self._settings_busy = settings_busy
        self.mute = MockIntVar(1)

    def change_state(self, state_number, msg):
        assert isinstance(state_number, int)
        state = GuiState(state_number)
        logger.debug(f" * changing state to {state}")
        self.changed.put_nowait((state, msg))
        self.state = GuiState(state)

    def set_signal_level(self, level):
        pass

    async def wait_that_state_is(self, expected_state: GuiState, timeout=1):
        try:
            state, msg = await asyncio.wait_for(self.changed.get(), timeout=timeout)
        except asyncio.TimeoutError:
            state = self.state
        assert_that(state, equal_to(expected_state), f"Wrong state in client {self.name}" if self.name else "")

    def open_settings(self, config):
        logger.debug(f" * open settings with {config}")

    def close_settings(self, force=False):
        if self._settings_busy and not force:
            return False
        logger.debug(" * sending GUI ready event")
        self.loop.call_soon_threadsafe(self.events_out.put_nowait, GuiEvent.GUI_READY.value)
        return True


class SlaveTestCase(AsyncTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.patcher1 = mock.patch('onlinechoir.slave.sound_card.sd.Stream', new=MockSounddeviceStream())
        cls.stream = cls.patcher1.start()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.patcher1.stop()

    def setUp(self) -> None:
        assert self.stream.n_instances == 0

    def tearDown(self) -> None:
        self.stream.reset()

    async def tick_as_received(self, clients, tasks, tick_counts):
        tick_counts = tuple(tick_counts)

        def clients_ready(n: int) -> bool:
            return all(not client._audio._input_queue.empty()
                       for client, n_ticks in zip(clients, tick_counts) if n < n_ticks)
        for i in range(max(tick_counts)):
            logger.debug(f" * wait for {i} packet to be in")
            await wait_until(partial(clients_ready, i))
            for task, n_ticks in zip(tasks, tick_counts):
                self.stream.instances[task][-1].tick()

    async def tick_all(self, tasks):
        for task in tasks:
            await self.stream.instances[task][-1].tick_all()
