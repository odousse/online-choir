import asyncio
import numpy as np
from hamcrest import assert_that, equal_to, greater_than_or_equal_to

from onlinechoir.server.audio_mixer import MIX_WINDOW_SIZE
from onlinechoir.common.constants import CHUNKS_IN_FLIGHT, SLAVE_PLAYBACK_BUFFER_SIZE, CHUNKS_PER_SECOND, CONNECTION_TIMEOUT, \
    FRAME_SIZE
from onlinechoir.common.auth import AuthenticationHeader
from onlinechoir.common.log import setup_logging
from onlinechoir.master.master_client import MasterClient, prepare_queue
from onlinechoir.server.server import MixServer, CHECK_IN_TIMEOUT
from onlinechoir.slave.gui_enums import GuiState, GuiEvent
from onlinechoir.slave.slave_client import SlaveClient
from .utils import GuiTracker, wait_until, SlaveTestCase

setup_logging('INFO', None)


def prepare_signal(n_chunks: int, skip_seconds=None) -> asyncio.Queue:
    sig = np.zeros((n_chunks * FRAME_SIZE, 2))
    q = prepare_queue(sig, skip_seconds)
    return q


class TestEndToEnd(SlaveTestCase):
    @staticmethod
    async def _create_clients(n: int, settings_busy=False):
        tasks = []
        clients = []
        trackers = []
        for i in range(n):
            gui_tracker = GuiTracker(settings_busy=settings_busy)
            latency = 10 + i
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': latency}, gui_tracker, True)
            task = asyncio.create_task(client.main())
            clients.append(client)
            trackers.append(gui_tracker)
            tasks.append(task)

        for gui_tracker in trackers:
            await gui_tracker.wait_that_state_is(GuiState.READY)

        return clients, trackers, tasks

    async def test_normal_sessions(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            clients, trackers, tasks = await self._create_clients(10)

            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(1000)))
            for i, (gui_tracker, task) in enumerate(zip(trackers, tasks)):
                await gui_tracker.wait_that_state_is(GuiState.PLAYING)
                await self.stream.set_latency(task, 10 + i)
            await self.tick_as_received(clients, tasks, range(1011, 1021))
            assert_that(server.mixer.num_active_sources, equal_to(10))

            for gui_tracker in trackers:
                await gui_tracker.wait_that_state_is(GuiState.READY)

            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(1000))

            # second session
            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(100)))
            for i, (gui_tracker, task) in enumerate(zip(trackers, tasks)):
                await gui_tracker.wait_that_state_is(GuiState.PLAYING)
                await self.stream.set_latency(task, 10 + i, 2)
            await self.tick_as_received(clients, tasks, range(111, 121))
            assert_that(server.mixer.num_active_sources, equal_to(10))

            for gui_tracker in trackers:
                await gui_tracker.wait_that_state_is(GuiState.READY)

            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(100))

            # kill clients
            for gui_tracker in trackers:
                gui_tracker.events_out.put_nowait(None)

            await asyncio.wait(tasks, timeout=1)

            await asyncio.sleep(1)
            assert_that(server.mixer.num_sources, equal_to(0))

    async def test_session_with_stale_client(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            clients, trackers, tasks = await self._create_clients(10)

            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(1000)))
            for i, (gui_tracker, task) in enumerate(zip(trackers, tasks)):
                await gui_tracker.wait_that_state_is(GuiState.PLAYING)
                await self.stream.set_latency(task, 10 + i)
            await self.tick_as_received(clients[:-1], tasks[:-1], range(1011, 1020))
            assert_that(server.mixer.num_active_sources, equal_to(10))

            for gui_tracker in trackers[:-1]:
                await gui_tracker.wait_that_state_is(GuiState.READY)
            assert_that(trackers[-1].state == GuiState.PLAYING)

            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(1000))

            await self.tick_as_received(clients[-1:], tasks[-1:], [1020])
            await trackers[-1].wait_that_state_is(GuiState.READY)

            # kill clients
            for gui_tracker in trackers:
                gui_tracker.events_out.put_nowait(None)

            await asyncio.wait(tasks, timeout=1)

            await asyncio.sleep(1)
            assert_that(server.mixer.num_sources, equal_to(0))

    async def test_short_session(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            clients, trackers, tasks = await self._create_clients(5)

            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(50)))
            for i, (gui_tracker, task) in enumerate(zip(trackers, tasks)):
                await gui_tracker.wait_that_state_is(GuiState.PLAYING)
                await self.stream.set_latency(task, 10 + i)
            await self.tick_as_received(clients, tasks, range(61, 66))
            assert_that(server.mixer.num_active_sources, equal_to(5))

            for gui_tracker in trackers:
                await gui_tracker.wait_that_state_is(GuiState.READY)

            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(50))

            # kill clients
            for gui_tracker in trackers:
                gui_tracker.events_out.put_nowait(None)

            await asyncio.wait(tasks, timeout=1)

            await asyncio.sleep(1)
            assert_that(server.mixer.num_sources, equal_to(0))

    async def test_short_session_with_stale_client(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            await asyncio.sleep(1)  # this is needed for the server to start correctly
            clients, trackers, tasks = await self._create_clients(5)

            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(50)))
            for i, (gui_tracker, task) in enumerate(zip(trackers, tasks)):
                await gui_tracker.wait_that_state_is(GuiState.PLAYING)
                await self.stream.set_latency(task, 10 + i)
            await self.tick_as_received(clients[:-1], tasks[:-1], range(61, 65))
            assert_that(server.mixer.num_active_sources, equal_to(5))

            for gui_tracker in trackers[:-1]:
                await gui_tracker.wait_that_state_is(GuiState.READY)

            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(50))

            await self.tick_as_received(clients[-1:], tasks[-1:], [65])
            await trackers[-1].wait_that_state_is(GuiState.READY)

            # kill clients
            for gui_tracker in trackers:
                gui_tracker.events_out.put_nowait(None)

            await asyncio.wait(tasks, timeout=1)

            await asyncio.sleep(1)
            assert_that(server.mixer.num_sources, equal_to(0))

    async def test_session_with_server_mix(self):
        async with MixServer(8878, 8868, 8858, safe=False, monitor_level=0.1) as server:
            clients, trackers, tasks = await self._create_clients(5)

            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(1000)))
            for i, (gui_tracker, task) in enumerate(zip(trackers, tasks)):
                await gui_tracker.wait_that_state_is(GuiState.PLAYING)
                await self.stream.set_latency(task, 10 + i)
            await self.tick_as_received(clients, tasks, range(1011, 1016))
            assert_that(server.mixer.num_active_sources, equal_to(6))

            for gui_tracker in trackers:
                await gui_tracker.wait_that_state_is(GuiState.READY)

            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(1000))

            # kill clients
            for gui_tracker in trackers:
                gui_tracker.events_out.put_nowait(None)

            await asyncio.wait(tasks, timeout=1)

            await asyncio.sleep(1)
            assert_that(server.mixer.num_sources, equal_to(0))

    async def test_one_calibrates_when_session_starts(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            clients, trackers, tasks = await self._create_clients(2, settings_busy=True)

            # start calibration on bad client
            trackers[1].events_out.put_nowait(GuiEvent.SETTINGS_BUTTON.value)
            await trackers[1].wait_that_state_is(GuiState.SETTINGS)

            # start session
            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(MIX_WINDOW_SIZE + 100)))
            await trackers[0].wait_that_state_is(GuiState.PLAYING)
            await self.stream.set_latency(tasks[0], 10)

            # tick good client
            await self.tick_as_received(clients[:1], tasks[:1], [MIX_WINDOW_SIZE + 11])

            # await that mix starts
            await wait_until(lambda: server.mixer.encoder is not None)
            assert_that(server.mixer.num_active_sources, equal_to(1))

            # tick more
            await self.tick_as_received(clients[:1], tasks[:1], [50])

            # close settings on bad client
            trackers[1].close_settings(force=True)
            await trackers[1].wait_that_state_is(GuiState.READY)
            assert_that(clients[1]._audio._input_queue.empty())  # all audio skipped

            # finish playing on good client
            await self.tick_as_received(clients[:1], tasks[:1], [50])
            await trackers[0].wait_that_state_is(GuiState.READY)
            assert_that(trackers[1].state, equal_to(GuiState.READY))
            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(MIX_WINDOW_SIZE + 100))

            # kill clients
            for gui_tracker in trackers:
                gui_tracker.events_out.put_nowait(None)

            await asyncio.wait(tasks, timeout=1)

            await asyncio.sleep(1)
            assert_that(server.mixer.num_sources, equal_to(0))

    async def test_no_client(self):
        async with MixServer(8878, 8868, 8858, safe=False):
            master = MasterClient(mute=True, monitor_level=0.0)
            await asyncio.wait_for(master.main('localhost', 8878, prepare_signal(400)), 1)
            assert_that(len(master.audio_in), equal_to(0))

    async def test_no_active_client(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            # connect to the server:
            reader, writer = await asyncio.open_connection('localhost', 8868)
            writer.write(AuthenticationHeader().to_bytes())
            await writer.drain()
            await wait_until(lambda: server.mixer.num_sources == 1)

            master = MasterClient(mute=True, monitor_level=0.0)
            await asyncio.wait_for(master.main('localhost', 8878, prepare_signal(400)), CHECK_IN_TIMEOUT + 1)
            assert_that(len(master.audio_in), equal_to(0))

    async def test_second_master(self):
        async with MixServer(8878, 8868, 8858, safe=False):
            gui_tracker = GuiTracker()
            client = SlaveClient({'server': 'localhost', 'port': 8868, 'latency': 10}, gui_tracker, True)
            task = asyncio.create_task(client.main())
            await gui_tracker.wait_that_state_is(GuiState.READY)

            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(50)))
            await gui_tracker.wait_that_state_is(GuiState.PLAYING)
            await self.stream.set_latency(task, 10)

            # second master
            master2 = MasterClient(mute=True, monitor_level=0.0)
            await asyncio.wait_for(master2.main('localhost', 8878, prepare_signal(50)), 1)  # should return immediately

            await self.tick_as_received([client], [task], [61])
            await gui_tracker.wait_that_state_is(GuiState.READY)
            await asyncio.wait_for(master_task, 1)

            # kill client
            gui_tracker.events_out.put_nowait(None)
            await asyncio.wait_for(task, 1)

    async def test_session_interrupted(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            clients, trackers, tasks = await self._create_clients(10)

            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(2000)))
            for i, (gui_tracker, task) in enumerate(zip(trackers, tasks)):
                await gui_tracker.wait_that_state_is(GuiState.PLAYING)
                await self.stream.set_latency(task, 10 + i)
            await self.tick_as_received(clients, tasks, [500] * 10)
            assert_that(server.mixer.num_active_sources, equal_to(10))

            for gui_tracker in trackers:
                assert_that(gui_tracker.state, equal_to(GuiState.PLAYING))

            # Interrupt. Server has received 500 - 20 = 480 frames, and has sent 480 + CHUNKS_IN_FLIGHT
            await asyncio.sleep(1)
            assert_that(len(master.audio_in), equal_to(481))
            master._stop()
            await asyncio.sleep(0.5)
            assert_that(master.encoder.next_index, equal_to(482 + CHUNKS_IN_FLIGHT))

            await self.tick_all(tasks)
            await asyncio.sleep(1)
            for gui_tracker in trackers:
                await gui_tracker.wait_that_state_is(GuiState.READY)

            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(482 + CHUNKS_IN_FLIGHT))

            # kill clients
            for gui_tracker in trackers:
                gui_tracker.events_out.put_nowait(None)

            await asyncio.wait(tasks, timeout=1)

            await asyncio.sleep(1)
            assert_that(server.mixer.num_sources, equal_to(0))

    async def test_client_buffer_size_stale(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            clients, trackers, tasks = await self._create_clients(2, settings_busy=True)

            # start calibration on bad client
            trackers[1].events_out.put_nowait(GuiEvent.SETTINGS_BUTTON.value)
            await trackers[1].wait_that_state_is(GuiState.SETTINGS)

            # start session
            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(CHUNKS_IN_FLIGHT + 100)))

            # tick good client slowly
            await trackers[0].wait_that_state_is(GuiState.PLAYING)
            await self.stream.set_latency(tasks[0], 10)
            min_buffer = np.inf
            for _ in range(MIX_WINDOW_SIZE + 100):
                await asyncio.sleep(0.01)
                min_buffer = min(min_buffer, clients[0]._audio._input_queue.qsize())
                self.stream.instances[tasks[0]][-1].tick()
            assert_that(server.mixer.num_active_sources, equal_to(1))
            await trackers[0].wait_that_state_is(GuiState.PLAYING)

            # check minimum buffer
            assert_that(min_buffer, greater_than_or_equal_to(SLAVE_PLAYBACK_BUFFER_SIZE + int(CHUNKS_PER_SECOND) - 11))

            # close settings on bad client
            trackers[1].close_settings(force=True)
            await trackers[1].wait_that_state_is(GuiState.READY)
            assert_that(clients[1]._audio._input_queue.empty())  # all audio skipped

            # complete session
            await self.tick_as_received(clients[:1], tasks[:1],
                                        [SLAVE_PLAYBACK_BUFFER_SIZE + int(CHUNKS_PER_SECOND) + 11])
            await trackers[0].wait_that_state_is(GuiState.READY)
            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(CHUNKS_IN_FLIGHT + 100))

            # kill clients
            for gui_tracker in trackers:
                gui_tracker.events_out.put_nowait(None)

            await asyncio.wait(tasks, timeout=1)

            await asyncio.sleep(1)
            assert_that(server.mixer.num_sources, equal_to(0))

    async def test_client_buffer_size_inactive(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            clients, trackers, tasks = await self._create_clients(1)

            # connect bad client:
            reader, writer = await asyncio.open_connection('localhost', 8868)
            writer.write(AuthenticationHeader().to_bytes())
            await writer.drain()
            await wait_until(lambda: server.mixer.num_sources == 2)

            # start session
            master = MasterClient(mute=True, monitor_level=0.0)
            master_task = asyncio.create_task(master.main('localhost', 8878, prepare_signal(CHUNKS_IN_FLIGHT + 100)))
            await trackers[0].wait_that_state_is(GuiState.PLAYING)
            await wait_until(lambda: clients[0]._audio._input_queue.qsize() >= CHUNKS_IN_FLIGHT)

            # tick good client
            await self.stream.set_latency(tasks[0], 10)
            min_buffer = np.inf
            for _ in range(MIX_WINDOW_SIZE + 100):
                await asyncio.sleep(0.01)
                min_buffer = min(min_buffer, clients[0]._audio._input_queue.qsize())
                self.stream.instances[tasks[0]][-1].tick()
            assert_that(server.mixer.num_active_sources, equal_to(1))
            await trackers[0].wait_that_state_is(GuiState.PLAYING)

            # check minimum buffer
            assert_that(server.mixer.num_active_sources, equal_to(1))
            assert_that(min_buffer, greater_than_or_equal_to(SLAVE_PLAYBACK_BUFFER_SIZE + int(CHUNKS_PER_SECOND) - 11))

            # complete session
            await self.tick_as_received(clients[:1], tasks[:1],
                                        [SLAVE_PLAYBACK_BUFFER_SIZE + int(CHUNKS_PER_SECOND) + 11])
            await trackers[0].wait_that_state_is(GuiState.READY)
            await asyncio.wait_for(master_task, 1)
            assert_that(len(master.audio_in), equal_to(CHUNKS_IN_FLIGHT + 100))

            # kill clients
            trackers[0].events_out.put_nowait(None)
            await asyncio.wait_for(tasks[0], timeout=1)
            writer.close()

            await wait_until(lambda: server.mixer.num_sources == 0)

    async def test_client_times_out(self):
        async with MixServer(8878, 8868, 8858, safe=False) as server:
            # connect to the server:
            reader, writer = await asyncio.open_connection('localhost', 8868)
            writer.write(AuthenticationHeader().to_bytes())
            await writer.drain()
            await wait_until(lambda: server.mixer.num_sources == 1)

            await asyncio.sleep(CONNECTION_TIMEOUT + 1)
            assert_that(server.mixer.num_sources, equal_to(0))
