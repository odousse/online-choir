import os
import numpy as np
from hamcrest import assert_that, equal_to, close_to

from onlinechoir.common.audio_codecs import AudioDecoder, AudioEncoder
from onlinechoir.common.constants import FRAME_SIZE


def test_encode_decode():
    idx = np.random.randint(-2 ** 23, 2 ** 23)

    encoder = AudioEncoder()
    encoder.next_index = idx
    decoder = AudioDecoder(idx)

    sample = 2 * np.random.random_sample(FRAME_SIZE) - 1
    data = encoder(sample)

    sig = np.empty((FRAME_SIZE,))
    decoder(data, sig)

    assert_that(decoder.peek_frame_index(data), equal_to(idx))
    assert_that(np.allclose(sig, sample, atol=0.6/32768))


def test_decode_encode():
    data = os.urandom(AudioDecoder.PACKET_SIZE - 4)
    idx = np.random.randint(-2 ** 23, 2 ** 23)
    data = AudioEncoder._prefix(idx) + data

    encoder = AudioEncoder()
    encoder.next_index = idx
    decoder = AudioDecoder(idx)

    sig = np.empty((FRAME_SIZE,))
    decoder(data, sig)

    data2 = encoder(sig)

    assert_that(data2, equal_to(data))


def test_saturation():
    encoder = AudioEncoder()
    decoder = AudioDecoder()

    sample = 1.1 * np.sin(np.linspace(0, 2 * np.pi, FRAME_SIZE))
    data = encoder(sample)

    sig = np.empty((FRAME_SIZE,))
    decoder(data, sig)

    expected = np.clip(sample, -1, 32767/32768)

    assert_that(np.allclose(sig, expected, atol=0.6/32768))
