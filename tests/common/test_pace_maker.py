import asyncio
import pytest
import time
from hamcrest import assert_that, close_to

from onlinechoir.common.pace_maker import PaceMaker


def test_fast():
    p = PaceMaker(0.05)
    start_time = time.time()
    for i in range(10):
        p.pace()
    elapsed = time.time() - start_time

    assert_that(elapsed, close_to(9 * 0.05, 0.01))


def test_fast_burst():
    p = PaceMaker(0.05, 5)
    start_time = time.time()
    for _ in range(10):
        p.pace()
    elapsed = time.time() - start_time

    assert_that(elapsed, close_to(4 * 0.05, 0.01))


def test_slow():
    p = PaceMaker(0.05)
    start_time = time.time()
    for _ in range(5):
        p.pace()
        time.sleep(0.1)
    elapsed = time.time() - start_time

    assert_that(elapsed, close_to(0.5, 0.03))


def test_mix():
    p = PaceMaker(0.05)
    start_time = time.time()
    for i in range(10):
        p.pace()
        if i % 5 == 1:
            time.sleep(0.1)
    elapsed = time.time() - start_time

    assert_that(elapsed, close_to(7 * 0.05 + 0.2, 0.03))


@pytest.mark.asyncio
async def test_fast_async():
    p = PaceMaker(0.05)
    start_time = time.time()
    for _ in range(10):
        await p.apace()
    elapsed = time.time() - start_time

    assert_that(elapsed, close_to(9 * 0.05, 0.01))


@pytest.mark.asyncio
async def test_fast_burst_async():
    p = PaceMaker(0.05, 5)
    start_time = time.time()
    for _ in range(10):
        await p.apace()
    elapsed = time.time() - start_time

    assert_that(elapsed, close_to(4 * 0.05, 0.01))


@pytest.mark.asyncio
async def test_slow_async():
    p = PaceMaker(0.05)
    start_time = time.time()
    for _ in range(5):
        await p.apace()
        await asyncio.sleep(0.1)
    elapsed = time.time() - start_time

    assert_that(elapsed, close_to(0.5, 0.03))


@pytest.mark.asyncio
async def test_mix_async():
    p = PaceMaker(0.05)
    start_time = time.time()
    for i in range(10):
        await p.apace()
        if i % 5 == 1:
            await asyncio.sleep(0.1)
    elapsed = time.time() - start_time

    assert_that(elapsed, close_to(7 * 0.05 + 0.2, 0.03))
