import os
import psutil
import time
from hamcrest import assert_that, calling, raises
from multiprocessing import Process, Event

from onlinechoir.common.singleton import register_process, unregister_process, _pid_file_path


def _other_process(name, event: Event):
    register_process(name)
    event.set()
    time.sleep(0.5)
    unregister_process(name)


def test_no_other_process():
    try:
        _pid_file_path('unit_test').unlink()
    except FileNotFoundError:
        pass

    register_process('unit_test')
    unregister_process('unit_test')

    assert_that(not _pid_file_path('unit_test').exists())


def test_other_process():
    event = Event()
    p = Process(target=_other_process, args=('unit_test', event))
    p.start()
    event.wait()
    assert_that(calling(register_process).with_args('unit_test'), raises(RuntimeError))
    p.join()
    assert_that(not _pid_file_path('unit_test').exists())


def test_other_name_process():
    event = Event()
    p = Process(target=_other_process, args=('unit_test2', event))
    p.start()
    event.wait()
    register_process('unit_test')
    unregister_process('unit_test')

    assert_that(not _pid_file_path('unit_test').exists())
    p.join()
    assert_that(not _pid_file_path('unit_test2').exists())


def test_other_process_dead():
    event = Event()
    p = Process(target=_other_process, args=('unit_test', event))
    p.start()
    p.join()
    register_process('unit_test')
    unregister_process('unit_test')

    assert_that(not _pid_file_path('unit_test').exists())


def test_other_process_dead_same_pid():
    pid = os.getpid()
    name = psutil.Process(pid).name()
    with _pid_file_path('unit_test').open('w') as fd:
        fd.write(f"{pid},{name}")

    register_process('unit_test')
    unregister_process('unit_test')

    assert_that(not _pid_file_path('unit_test').exists())


def test_register_twice():  # same as above...
    register_process('unit_test')
    register_process('unit_test')
    unregister_process('unit_test')

    assert_that(not _pid_file_path('unit_test').exists())


def test_bad_file():
    with _pid_file_path('unit_test').open('w') as fd:
        fd.write('bla')

    register_process('unit_test')
    unregister_process('unit_test')

    assert_that(not _pid_file_path('unit_test').exists())


def test_other_process_with_different_name():
    with _pid_file_path('unit_test').open('w') as fd:
        fd.write(f"333,bla")

    register_process('unit_test')
    unregister_process('unit_test')

    assert_that(not _pid_file_path('unit_test').exists())
