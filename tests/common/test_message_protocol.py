from hamcrest import assert_that, equal_to

from onlinechoir.common.message_protocol import MessageProtocol


def test_consistency():
    msg = {'text': 'Et voilà'}
    data = MessageProtocol(msg).to_bytes()

    assert_that(MessageProtocol.get_length(data[:2]), equal_to(len(data) - 2))
    assert_that(MessageProtocol.from_bytes(data[2:]).msg, equal_to(msg))
