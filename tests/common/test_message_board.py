from collections import defaultdict
from threading import Thread
from time import sleep

from hamcrest import assert_that, equal_to, calling, raises, is_in, has_length

from onlinechoir.common.message_board import MessageBoard


def test_one_message():
    board = MessageBoard()
    board.post_message('topic', 'msg')

    topic, message = board.get_message(1)
    assert_that(topic, equal_to('topic'))
    assert_that(message, equal_to('msg'))

    assert_that(calling(board.get_message).with_args(0.1), raises(TimeoutError))


def test_overwriting_message():
    board = MessageBoard()
    board.post_message('topic', 'msg')
    board.post_message('topic', 'msg2')

    topic, message = board.get_message(1)
    assert_that(topic, equal_to('topic'))
    assert_that(message, equal_to('msg2'))

    assert_that(calling(board.get_message).with_args(0.1), raises(TimeoutError))


def test_one_message_iter():
    board = MessageBoard()
    board.post_message('topic', 'msg')

    l = []

    def list_messages():
        for topic, message in board.messages(1):
            l.append((topic, message))

    thread = Thread(target=list_messages)
    thread.start()

    board.close()
    thread.join(1)

    assert_that(l, equal_to([('topic', 'msg')]))


def test_overwriting_message_iter():
    board = MessageBoard()
    board.post_message('topic', 'msg')
    board.post_message('topic', 'msg2')

    l = []

    def list_messages():
        for topic, message in board.messages(1):
            l.append((topic, message))

    thread = Thread(target=list_messages)
    thread.start()

    board.close()
    thread.join(1)

    assert_that(l, equal_to([('topic', 'msg2')]))


def test_get_several_messages():
    board = MessageBoard()
    msgs = {'topic1': 'msg', 'topic2': 'msg', 'topic3': 'msg'}
    board.post_messages(msgs)

    for _ in range(3):
        topic, message = board.get_message(1)
        assert_that(topic, is_in(msgs))
        assert_that(message, equal_to(msgs.pop(topic)))

    assert_that(calling(board.get_message).with_args(0.1), raises(TimeoutError))


def test_get_several_messages_iter():
    board = MessageBoard()
    msgs = {'topic1': 'msg', 'topic2': 'msg', 'topic3': 'msg'}
    board.post_messages(msgs)

    l = []

    def list_messages():
        for topic, message in board.messages(1):
            l.append((topic, message))

    thread = Thread(target=list_messages)
    thread.start()

    board.close()
    thread.join(1)

    assert_that(dict(l), equal_to(msgs))


def test_get_several_interleaved_messages_iter():
    board = MessageBoard()
    msgs = {'topic1': 'msg', 'topic2': 'msg', 'topic3': 'msg'}
    board.post_messages(msgs)

    l = defaultdict(list)

    def list_messages():
        for topic, message in board.messages(1):
            l[topic].append(message)

    thread = Thread(target=list_messages)
    thread.start()

    sleep(0.1)
    board.post_message('topic2', 'msg2')
    sleep(0.1)

    board.close()
    thread.join(1)

    assert_that(l, has_length(3))
    assert_that(l['topic1'], equal_to(['msg']))
    assert_that(l['topic2'], equal_to(['msg', 'msg2']))
    assert_that(l['topic3'], equal_to(['msg']))
