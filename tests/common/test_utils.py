import asyncio
import pytest
from hamcrest import assert_that

from onlinechoir.common.utils import iterate_queue, flush_queue


@pytest.mark.asyncio
async def test_queue_iterator_normal():
    q = asyncio.Queue()
    for item in ('a', 'b', 'c'):
        q.put_nowait(item)
    q.put_nowait(None)

    res = []
    async for item in iterate_queue(q):
        res.append(item)

    assert_that(res == ['a', 'b', 'c'])
    await q.join()


@pytest.mark.asyncio
async def test_queue_iterator_empty():
    q = asyncio.Queue()
    q.put_nowait(None)
    q.put_nowait('d')

    res = []
    async for item in iterate_queue(q):
        res.append(item)

    assert_that(res == [])
    flush_queue(q)
    await q.join()
