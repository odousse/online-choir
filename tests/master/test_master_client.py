import asyncio
import numpy as np
import pytest
from hamcrest import assert_that, equal_to

from onlinechoir.common.constants import CHUNKS_IN_FLIGHT, FRAME_SIZE
from onlinechoir.common.log import setup_logging
from onlinechoir.common.utils import iterate_queue
from onlinechoir.master.master_client import MasterClient, prepare_queue
from ..utils import MockEchoServer, silence_chunk

setup_logging('DEBUG', None)


def prepare_signal(n_chunks: int, skip_seconds=None) -> asyncio.Queue:
    sig = np.zeros((n_chunks * FRAME_SIZE, 2))
    q = prepare_queue(sig, skip_seconds)
    return q


@pytest.mark.asyncio
async def test_prepare_queue1():
    sig = np.zeros((500 * FRAME_SIZE, 2))
    q = prepare_queue(sig, 2.)
    i = 0
    async for chunk in iterate_queue(q):
        i += 1
        assert_that(chunk.shape, equal_to((FRAME_SIZE, 2)))
    assert_that(i, equal_to(500 - 172))


@pytest.mark.asyncio
async def test_prepare_queue2():
    sig = np.zeros((500 * FRAME_SIZE, 2))
    q = prepare_queue(sig, 0.356)
    i = 0
    async for chunk in iterate_queue(q):
        i += 1
        assert_that(chunk.shape, equal_to((FRAME_SIZE, 2)))
    assert_that(i, equal_to(500 - 31))


@pytest.mark.asyncio
async def test_normal_session():
    async with MockEchoServer(CHUNKS_IN_FLIGHT) as test_server:
        client = MasterClient(mute=True, monitor_level=0.1)
        signal = prepare_signal(1000)
        client_logic = asyncio.create_task(client.main('localhost', 8878, signal))
        await asyncio.sleep(1)
        assert_that(test_server.master_present)
        assert_that(signal.empty())  # all chunks sent
        test_server.pass_frames(CHUNKS_IN_FLIGHT)
        await asyncio.wait_for(client_logic, 1)
        assert_that(test_server.session_complete)
        assert_that(test_server.count, equal_to(1000))


@pytest.mark.asyncio
async def test_session_too_many_packets_in_flight():
    async with MockEchoServer(CHUNKS_IN_FLIGHT + 1) as test_server:
        client = MasterClient(mute=True, monitor_level=0.1)
        signal = prepare_signal(1000)
        client_logic = asyncio.create_task(client.main('localhost', 8878, signal))
        await asyncio.sleep(1)
        assert_that(test_server.master_present)
        assert_that(signal.qsize(), equal_to(1000 - CHUNKS_IN_FLIGHT - 1))  # deadlock, too many chunks in flight
        test_server.pass_frames(1000)
        await asyncio.wait_for(client_logic, 1)
        assert_that(test_server.session_complete)
        assert_that(test_server.count, equal_to(1000))


@pytest.mark.asyncio
async def test_session_too_many_packets_in_flight2():
    async with MockEchoServer(0) as test_server:
        client = MasterClient(mute=True, monitor_level=0.1)
        signal = prepare_signal(1000)
        client_logic = asyncio.create_task(client.main('localhost', 8878, signal))
        await asyncio.sleep(1)
        assert_that(test_server.master_present)
        assert_that(signal.qsize(), equal_to(1000 - CHUNKS_IN_FLIGHT - 1))  # deadlock, too many chunks in flight
        test_server.pass_frames(1000)
        await asyncio.wait_for(client_logic, 1)
        assert_that(test_server.session_complete)
        assert_that(test_server.count, equal_to(1000))


@pytest.mark.asyncio
async def test_session_interrupted():
    async with MockEchoServer(0) as test_server:
        client = MasterClient(mute=True, monitor_level=0.1)
        client_logic = asyncio.create_task(client.main('localhost', 8878, prepare_signal(1000)))
        await asyncio.sleep(0.5)
        assert_that(test_server.master_present)
        test_server.pass_frames(100)
        await asyncio.sleep(0.5)
        client._stop()
        test_server.pass_frames(CHUNKS_IN_FLIGHT + 10)
        await asyncio.wait_for(client_logic, 2)  # TODO: find a better solution...
        assert_that(test_server.session_complete)


@pytest.mark.asyncio
async def test_skip_seconds():
    async with MockEchoServer(CHUNKS_IN_FLIGHT) as test_server:
        client = MasterClient(mute=True, monitor_level=0.1)
        client_logic = asyncio.create_task(client.main('localhost', 8878, prepare_signal(1000, 5.12)))
        await asyncio.sleep(1)
        assert_that(test_server.master_present)
        test_server.pass_frames(CHUNKS_IN_FLIGHT)
        await asyncio.wait_for(client_logic, 1)
        assert_that(test_server.session_complete)
        assert_that(test_server.count, equal_to(1000 - 441))


@pytest.mark.asyncio
async def test_live_stream():
    async with MockEchoServer(CHUNKS_IN_FLIGHT) as test_server:
        client = MasterClient(mute=True, monitor_level=0.1)
        signal = asyncio.Queue()
        client_logic = asyncio.create_task(client.main('localhost', 8878, signal))
        await asyncio.sleep(0.5)
        assert_that(test_server.master_present)
        for _ in range(1000):
            await signal.put(silence_chunk())
        await asyncio.sleep(1)
        assert_that(signal.empty())
        client._stop()
        await signal.put(silence_chunk())
        test_server.pass_frames(CHUNKS_IN_FLIGHT + 10)
        await asyncio.wait_for(client_logic, 1)
        assert_that(test_server.session_complete)
        assert_that(test_server.count, equal_to(1000))
