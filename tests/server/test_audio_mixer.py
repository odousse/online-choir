import audioop
import asyncio
import numpy as np
import pytest
from hamcrest import assert_that, equal_to, less_than

from onlinechoir.common.audio_codecs import AdpcmEncoder, AudioDecoder, AudioEncoder
from onlinechoir.common.constants import SAMPLING_RATE, FRAME_SIZE
from onlinechoir.common.log import setup_logging
from onlinechoir.server.audio_mixer import AudioMixer, MIX_WINDOW_SIZE

setup_logging('INFO', None)


async def assert_that_mix_frame_is_produced(mixer: AudioMixer, idx: int):
    data = await asyncio.wait_for(mixer.get_frame(), 1)
    assert_that(AudioDecoder.peek_frame_index(data), equal_to(idx))


@pytest.mark.asyncio
async def test_mix_all_good():
    mixer = AudioMixer()

    # register
    for s in range(20):
        mixer.register_source((s,))

    # check-in
    for s in range(20):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(-1))

    for idx in range(1000):
        if idx == 900:
            mixer.set_end_frame(999)
        for s in range(20):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))
        await assert_that_mix_frame_is_produced(mixer, idx)


@pytest.mark.asyncio
async def test_mix_all_good_some_missing():
    mixer = AudioMixer()

    # register
    for s in range(20):
        mixer.register_source((s,))

    # check-in all but one
    for s in range(19):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(-1))

    # send some audio
    for idx in range(MIX_WINDOW_SIZE):
        for s in range(19):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))
        assert_that(mixer.mix_queue.empty())

    # send one more frame
    for s in range(19):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(MIX_WINDOW_SIZE))

    # check that mix has started
    for idx in range(MIX_WINDOW_SIZE + 1):
        await assert_that_mix_frame_is_produced(mixer, idx)

    # late check-in
    mixer.add_frame((19,), AdpcmEncoder.silence_frame(-1))
    assert_that(mixer.num_active_sources, equal_to(19))  # not accepted

    # late audio
    for idx in range(MIX_WINDOW_SIZE + 1):
        mixer.add_frame((19,), AdpcmEncoder.silence_frame(idx))

    # Finish session
    for idx in range(MIX_WINDOW_SIZE + 1, 500):
        for s in range(20):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))
        await assert_that_mix_frame_is_produced(mixer, idx)


@pytest.mark.asyncio
async def test_mix_all_good_some_missing_short():
    mixer = AudioMixer()

    # register
    for s in range(20):
        mixer.register_source((s,))

    # check-in all but one
    for s in range(19):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(-1))

    # send some audio
    for idx in range(25):
        for s in range(19):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))
        assert_that(mixer.mix_queue.empty())

    # set end frame
    mixer.set_end_frame(49)

    # send some audio
    for idx in range(25, 50):
        for s in range(19):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))

    # check that mix has started
    for idx in range(50):
        await assert_that_mix_frame_is_produced(mixer, idx)


@pytest.mark.asyncio
async def test_mix_late_sources():
    """
    Tests
    """
    mixer = AudioMixer()

    # register
    for s in range(20):
        mixer.register_source((s,))

    # check-in
    for s in range(20):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(-1))

    for idx in range(500):
        for s in range(16):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))

    for idx in range(500 - MIX_WINDOW_SIZE):
        await assert_that_mix_frame_is_produced(mixer, idx)

    # catch up with other sources
    for idx in range(500):
        for s in range(16, 20):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))
        if idx >= 500 - MIX_WINDOW_SIZE:
            await assert_that_mix_frame_is_produced(mixer, idx)


@pytest.mark.asyncio
async def test_late_join():
    mixer = AudioMixer()

    for s in range(19):
        mixer.register_source((s,))

    for s in range(19):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(-1))

    for idx in range(500):
        for s in range(19):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))

    mixer.register_source(('new_comer',))
    mixer.add_frame(('new_comer',), AdpcmEncoder.silence_frame(-1))  # registration should be ignored

    for idx in range(500, 600):
        for s in range(19):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))

    for idx in range(600):
        await assert_that_mix_frame_is_produced(mixer, idx)


@pytest.mark.asyncio
async def test_early_connection_drop():
    mixer = AudioMixer()

    for s in range(20):
        mixer.register_source((s,))

    for s in range(20):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(-1))

    for idx in range(500):
        for s in range(20):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))

    mixer.unregister_source((0,))

    for idx in range(500, 600):
        for s in range(1, 20):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))

    for idx in range(600):
        await assert_that_mix_frame_is_produced(mixer, idx)


@pytest.mark.asyncio
async def test_early_session_drop():
    mixer = AudioMixer()

    for s in range(20):
        mixer.register_source((s,))

    for s in range(20):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(-1))

    for idx in range(500):
        for s in range(20):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))

    mixer.add_frame((0,), AdpcmEncoder.silence_frame(-2))  # drop

    for idx in range(500, 600):
        for s in range(1, 20):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))

    for idx in range(600):
        await assert_that_mix_frame_is_produced(mixer, idx)


@pytest.mark.asyncio
async def test_no_active_source_left():
    mixer = AudioMixer()

    # register
    for s in range(20):
        mixer.register_source((s,))

    # check-in
    for s in range(20):
        mixer.add_frame((s,), AdpcmEncoder.silence_frame(-1))

    first_active_source = 0
    for idx in range(199):
        if (idx - 9) % 10 == 0:
            mixer.add_frame((first_active_source,), AdpcmEncoder.silence_frame(-2))  # drop
            first_active_source += 1
        for s in range(first_active_source, 20):
            mixer.add_frame((s,), AdpcmEncoder.silence_frame(idx))
        await assert_that_mix_frame_is_produced(mixer, idx)
    mixer.add_frame((19,), AdpcmEncoder.silence_frame(-2))  # drop last
    await assert_that_mix_frame_is_produced(mixer, -2)
    assert_that(mixer.mix_queue.empty())


@pytest.mark.asyncio
async def test_two_slaves_join_and_leave():
    mixer = AudioMixer()
    mixer.register_source((1,))
    mixer.register_source((2,))

    mixer.unregister_source((1,))
    assert_that(mixer.mix_queue.empty())
    mixer.unregister_source((2,))
    assert_that(mixer.mix_queue.empty())
    assert_that(mixer.num_sources, equal_to(0))


def test_audio_mixing():
    encoder = AudioEncoder()

    def create_sine(freq):
        return np.sin(np.linspace(0, 2 * np.pi * freq * FRAME_SIZE / SAMPLING_RATE, FRAME_SIZE))

    a = create_sine(300)
    b = create_sine(1000)
    c = create_sine(5000)

    m = (a + b + c) / 3

    ab = audioop.lin2lin(encoder._encode_signal(a), 2, 3)
    bb = audioop.lin2lin(encoder._encode_signal(b), 2, 3)
    cb = audioop.lin2lin(encoder._encode_signal(c), 2, 3)

    mb = AudioMixer.mix_frames(((1, ab), (1, bb), (1, cb)), 3)
    r = np.empty((FRAME_SIZE,))

    AudioDecoder._to_array(mb, r, 0)

    assert_that(max(np.abs(r - m)), less_than(0.0001))
