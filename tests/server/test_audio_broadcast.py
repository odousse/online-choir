import asyncio

import pytest
from hamcrest import assert_that, equal_to

from onlinechoir.common.audio_codecs import StereoEncoder, StereoDecoder
from onlinechoir.common.constants import FrameType
from onlinechoir.server.audio_broadcaster import AudioBroadcaster


@pytest.mark.asyncio
async def test_all_good():
    broadcaster = AudioBroadcaster()

    # register
    for s in range(5):
        broadcaster.register_client(str(s))

    for idx in range(100):
        broadcaster.add_input_frame(StereoEncoder.silence_frame(idx))
        for s in range(3):
            await asyncio.wait_for(broadcaster.get_frame(str(s)), 1)

    broadcaster.add_input_frame(StereoEncoder.silence_frame(FrameType.EOF))
    for s in range(3):
        data = await asyncio.wait_for(broadcaster.get_frame(str(s)), 1)
        assert_that(StereoDecoder.peek_frame_index(data), equal_to(FrameType.EOF))

    for idx in range(101):
        for s in range(3, 5):
            await asyncio.wait_for(broadcaster.get_frame(str(s)), 1)
