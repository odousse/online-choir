import asyncio
import json
import numpy as np
import sys
import wave
from io import BytesIO
from flask import Flask, make_response, send_file
from pathlib import Path
from werkzeug.exceptions import abort

project_root = str(Path(__file__).parent.resolve().parent / "src")
if project_root not in sys.path:
    sys.path.append(project_root)

from onlinechoir.master.master_client import prepare_queue, MasterClient

app = Flask(__name__)
status = {
    'recording': None,
    'master': None

}


def load_audio(fname):
    with wave.open(fname) as f:
        n = f.getnframes()
        data = f.readframes(n)
    n_chunks = ((n - 1) // 512) + 1
    sig = np.frombuffer(data, dtype='int16') / 32768
    res = np.zeros((n_chunks * 512, 2), dtype='float32', order='C')
    res[:n, 0] = sig
    res[:n, 1] = sig
    return res


click = load_audio(project_root + '/../assets/sound/click.wav')


# rendering the HTML page which has the button
@app.route('/')
def get_main_page():
    return send_file('index.html')


@app.route('/index.js')
def get_js():
    return send_file('index.js')


@app.route('/style.css')
def get_css():
    return send_file('style.css')


# background process happening without any refreshing
@app.route('/play')
def background_process_test():
    if status['master']:
        res = 'Busy'
    else:
        master = MasterClient(mute=True, monitor_level=0)
        status['master'] = master
        res = "Session failed"
        try:
            asyncio.run(run_master(master))
            rec = master.get_recording()
            if rec is not None and len(rec) == len(click):
                status['recording'] = sig2wav(rec * 0.8 + click.mean(axis=1) * 0.2)
                res = "Session complete"
            else:
                status['recording'] = None
        finally:
            status['master'] = None

    response = make_response(json.dumps({'contents': res}))
    response.headers.set('Content-Type', 'application/json')
    return response


@app.route('/recording.wav')
def get_recording():
    if not status['recording']:
        abort(410)
    response = make_response(status['recording'])
    response.headers.set('Content-Type', 'audio/wave')
    response.headers.set('Content-Disposition', 'attachment', filename='recording.wav')
    return response


async def run_master(master: MasterClient):
    q = prepare_queue(click, 0.)
    await master.main('online-choir.modetexte.ch', 8878, q)
    return


def sig2wav(sig: np.ndarray) -> bytes:
    assert len(sig.shape) == 1
    bounded = np.clip(sig * 32768, -32768, 32767)
    digitalized = np.rint(bounded).astype('int16')
    data = BytesIO()
    with wave.open(data, 'wb') as f:
        f.setnchannels(1)
        f.setsampwidth(2)
        f.setframerate(44100)
        f.writeframes(digitalized.tobytes())
    return data.getvalue()
