// selecting dom element
//const textInput = document.querySelector("#inputPart");
const textOutput = document.querySelector("#showOutput");
const btn = document.querySelector("#submitInput");

// adding event listener to button
btn.addEventListener("click", fetchHandler);

// selecting loading div
const loader = document.querySelector("#loading");

// showing loading
function displayLoading() {
    loader.classList.add("display");
    // to stop loading after some time
    setTimeout(() => {
        loader.classList.remove("display");
    }, 10000);
}

// hiding loading 
function hideLoading() {
    loader.classList.remove("display");
}

// dummy url
var url = "play"

function fetchHandler(event) {
    displayLoading()
    //var input = textInput.value;
    //var finalURL = buildURL(input);

    fetch(url)
        .then(response => response.json())
        .then(json => {
            console.log(json)
            hideLoading()
            textOutput.innerText = json.contents;
        })
}
// creating url format
// we need 
// https://lessonfourapi.tanaypratap.repl.co/translate/yoda.json?text="your input"

//function buildURL(inputData) {
    //return `${url}?text=${inputData}`;
//    return `${url}`;
//}
