# Web service

This folder contains a web interface that allows users to test the configuration of their slave client.
The web interface is a very limited interface to the master client, so that they don't need another user to run it for 
testing purpose.

## Light deployment

The web page is programmed using flask. 
Therefore, the app can simply be run using the built-in flask web server.
It is however not recommended using it in production.

```shell
export FLASK_APP=web/self_check.py 
flask run --host=0.0.0.0
```
Debug options are available. 
Please refer to [flask's documentation](https://flask.palletsprojects.com/en/1.1.x/quickstart/#debug-mode).

## Production deployment

All deployment methods are also documented [here](https://flask.palletsprojects.com/en/1.1.x/deploying/#deployment).
We give some more instructions on how to do it using Apache below.

### Deployment using Apache

Create a `wsgi` file with the following content
```python
import sys
code_path = '<absolute path to the source of online-choir>'
sys.path.insert(0, code_path)
from src.onlinechoir.common.log import setup_logging
from web.self_check import app as application
setup_logging('INFO')
```
and put it somewhere where apache can read it, for instance `/var/www/online-choir/self_check.wsgi`.

Then add the following to the configuration of your web-site:
```
    WSGIDaemonProcess self_check user=<some user for the deamon> group=www-data threads=1
    WSGIScriptAlias /online-choir/self-test /var/www/online-choir/self_check.wsgi
    <Directory /var/www/online-choir>
        WSGIProcessGroup self_check
        WSGIApplicationGroup %{GLOBAL}
        Order deny,allow
        Allow from all
    </Directory>
```