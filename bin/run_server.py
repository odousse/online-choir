import sys
from pathlib import Path

project_root = str(Path(__file__).parent.resolve().parent / "src")
if project_root not in sys.path:
    sys.path.append(project_root)

from onlinechoir.server import cli
cli()
