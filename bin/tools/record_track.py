import soundfile
import numpy as np
import sounddevice
import threading
from datetime import datetime


class Recorder:
    def __init__(self):
        self.frames = []
        self.done = threading.Event()

    def _callback(self, indata: np.ndarray, frames: int, _, status):
        frame = np.empty((512,), dtype=np.float32)
        frame[:] = indata[:, 0]
        self.frames.append(frame)

    def _done(self):
        self.done.set()

    def main(self):
        stream = sounddevice.InputStream(samplerate=44100, blocksize=512, channels=1, callback=self._callback,
                                         finished_callback=self._done)
        stream.start()
        self.done.wait()

    def write_file(self, fname):
        print(len(self.frames))
        print(self.frames[0].shape)
        track = np.concatenate(self.frames)
        print(track.shape)
        soundfile.write(fname, track, 44100)


def cli():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output")
    parsed_args = parser.parse_args()

    fname = parsed_args.output
    if fname is None:
        ts = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        fname = f"track_{ts}.wav"
    rec = Recorder()
    try:
        rec.main()
    except KeyboardInterrupt:
        pass
    rec.write_file(fname)


if __name__ == '__main__':
    cli()
