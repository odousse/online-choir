import asyncio
import numpy as np
import time
from functools import partial
from typing import Optional

from onlinechoir.common.audio_codecs import StereoDecoder, AudioEncoder
from onlinechoir.common.constants import FRAME_SIZE
from onlinechoir.common.auth import AuthenticationHeader, HEADER_SIZE
from onlinechoir.common.log import parse_args_and_set_logger, logger
from onlinechoir.common.utils import iterate_stream


async def echo_server(reader, writer, rate: Optional[float], init_delay: float):
    addr = writer.get_extra_info('peername')
    logger.info(f"Connected to {addr}")
    header = AuthenticationHeader.from_bytes(await reader.readexactly(HEADER_SIZE))
    if not header.is_valid:
        logger.error("Invalid header")
        writer.close()
        return
    decoder = StereoDecoder()
    encoder = AudioEncoder()
    sig = np.empty((FRAME_SIZE, 2), dtype=np.float32)
    interval = 1 / rate if rate else None
    start_time = None
    i = 0
    async for data in iterate_stream(reader, StereoDecoder.PACKET_SIZE):
        if init_delay:
            await asyncio.sleep(init_delay)
            init_delay = 0
        if rate:  # rate control
            if start_time is None:
                start_time = time.time()
            advance = start_time + i * interval - time.time()
            if advance > 0:
                await asyncio.sleep(advance)
            i += 1

        idx = StereoDecoder.peek_frame_index(data)
        if idx >= 0:
            decoder(data, sig)
            logger.debug(f"  forwarding frame {idx}, var={sig.var()}")
            writer.write(encoder(sig.mean(axis=1)))
            await writer.drain()
        else:
            logger.debug("  received end frame, not forwarding.")
            encoder = AudioEncoder()
            decoder = StereoDecoder()
    writer.close()
    logger.info(f"Disconnected from {addr}")


async def main(port_number: int, rate: Optional[float], init_delay: float):
    server = await asyncio.start_server(partial(echo_server, rate=rate, init_delay=init_delay), port=port_number)

    addr = server.sockets[0].getsockname()
    logger.info(f'Serving on {addr}')

    async with server:
        await server.serve_forever()


def cli():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port-number', type=int, default=8878)
    parser.add_argument('-r', '--rate', type=float, help="Limit the rate of forwarding (in chunks per second)")
    parser.add_argument('-i', '--init-delay', type=float, default=0, help="Initial delay before packets are echoed")
    parsed_args = parse_args_and_set_logger(parser)

    asyncio.run(main(parsed_args.port_number, parsed_args.rate, parsed_args.init_delay))

    exit(0)


if __name__ == '__main__':
    cli()
