import asyncio
import time

import numpy as np
from functools import partial
from typing import Optional

import soundfile

from onlinechoir.common.audio_codecs import StereoEncoder, AdpcmDecoder
from onlinechoir.common.audio_files import load_audio_file
from onlinechoir.common.auth import AuthenticationHeader, HEADER_SIZE
from onlinechoir.common.constants import FRAME_SIZE, FrameType
from onlinechoir.common.log import parse_args_and_set_logger, logger


async def play_server(reader, writer, sig: np.ndarray, rate: Optional[float], out_file: Optional[str] = None):
    interval = 1 / rate if rate else 0.
    addr = writer.get_extra_info('peername')
    logger.info(f"Connected to {addr}")
    header = AuthenticationHeader.from_bytes(await reader.readexactly(HEADER_SIZE))
    if not header.is_valid:
        logger.error("Invalid header")
        writer.close()
        return
    rec = np.zeros((len(sig),), dtype=np.float32)

    try:
        while True:
            input("Press Enter to continue...")
            encoder = StereoEncoder()
            decoder = AdpcmDecoder()
            next_send = 0.0
            for i in range(0, len(sig), FRAME_SIZE):
                t = time.time()
                if t < next_send:
                    await asyncio.sleep(next_send - t)
                next_send = t + interval
                data = encoder(sig[i: i + FRAME_SIZE, :])
                writer.write(data)
                await writer.drain()
                if i == 0:
                    logger.info("Sent first chunk, wait for check-in")
                    idx = FrameType.KEEPALIVE
                    while idx == FrameType.KEEPALIVE:
                        data = await reader.readexactly(decoder.PACKET_SIZE)
                        idx = decoder.peek_frame_index(data)
                        logger.info(f"got {idx}")
                    assert idx == -1
                    logger.info("Got check in")
            logger.info("Send end of file")
            writer.write(encoder.silence_frame(-1))
            await writer.drain()

            # decode the received signal
            logger.info("Reading result")
            i = 0
            while i < len(sig):
                data = await reader.readexactly(decoder.PACKET_SIZE)
                idx = decoder.peek_frame_index(data)
                if idx == FrameType.KEEPALIVE:
                    continue
                if idx == FrameType.ABORT:
                    logger.info("Got abort packet")
                    break
                decoder(data, rec[i:i + FRAME_SIZE])
                i += FRAME_SIZE
    except KeyboardInterrupt:
        pass

    writer.close()
    logger.info(f"Disconnected from {addr}")
    if out_file:
        soundfile.write(out_file, rec, 44100)


async def main(file_name: str, port_number: int, rate: Optional[float], output_file: Optional[str]):
    sig = load_audio_file(file_name)

    server = await asyncio.start_server(partial(play_server, sig=sig, rate=rate, out_file=output_file),
                                        port=port_number)

    addr = server.sockets[0].getsockname()
    logger.info(f'Serving on {addr}')

    async with server:
        await server.serve_forever()


def cli():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port-number', type=int, default=8868)
    parser.add_argument('-r', '--rate', type=float, help="Limit the rate of forwarding (in chunks per second)")
    parser.add_argument('-o', '--output-file', help="The file int owhich to record")
    parser.add_argument('file_name', help="The file to play")
    parsed_args = parse_args_and_set_logger(parser)

    asyncio.run(main(parsed_args.file_name, parsed_args.port_number, parsed_args.rate, parsed_args.output_file))

    exit(0)


if __name__ == '__main__':
    cli()
