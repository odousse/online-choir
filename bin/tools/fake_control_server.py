import asyncio
from dataclasses import dataclass
from functools import partial
from random import randint, randrange, random
from typing import Tuple, Sequence, Dict, Any, Optional, List

from onlinechoir.common.auth import AuthenticationHeader, HEADER_SIZE
from onlinechoir.common.constants import VOCAL_GROUPS
from onlinechoir.common.log import logger, parse_args_and_set_logger
from onlinechoir.common.message_protocol import MessageProtocol


@dataclass
class Source:
    address: Tuple
    name: str
    group: str
    gain: float


def process_command(command: Dict[str, Any], clients: Sequence[Source],
                    volatility: float, presence: float) -> Optional[List]:
    action = command.get('action', 'none')
    if action == 'list':
        sources = []
        for client in clients:
            if random() < volatility:
                logger.info(f"Rotating address of {client.name}")
                client.address = (f"addr{randint(0, 1000)}", randint(0, 1000))
                client.gain = 1.0
            if random() < presence:
                sources.append({'id': client.address, 'name': client.name, 'group': client.group,
                                'gain': client.gain})
        logger.info(f"Sent list with {len(sources)} sources")
        return sources
    elif action == 'adjust_gain':
        source_id = tuple(command.get('id', ()))
        target = None
        for client in clients:
            if client.address == source_id:
                target = client
                break
        if target is None:
            logger.error(f"bad target {source_id}")
            return None
        try:
            gain = float(command['gain'])
        except (ValueError, KeyError):
            logger.error("invalid gain")
            return None
        logger.info(f"change gain of source {source_id} to {gain}")
        target.gain = gain
        return None


async def control_server(reader, writer, clients: Sequence[Source], volatility: float, presence: float):
    addr = writer.get_extra_info('peername')
    logger.info(f"Connected to {addr}")
    header = AuthenticationHeader.from_bytes(await reader.readexactly(HEADER_SIZE))
    if not header.is_valid:
        logger.error("Invalid header")
        writer.close()
        return

    while True:
        try:
            l = await reader.readexactly(2)
            msg = MessageProtocol.from_bytes(await reader.readexactly(MessageProtocol.get_length(l)))
            logger.debug(f"Received command {msg.msg}")
        except (ConnectionResetError, ValueError, asyncio.IncompleteReadError):
            break
        response = process_command(msg.msg, clients, volatility, presence)
        if response is not None:
            writer.write(MessageProtocol(response).to_bytes())
            await writer.drain()
    writer.close()
    logger.info(f"Disconnected from {addr}")


async def main(port_number: int, n_clients: int, volatility: float, presence: float):
    sources = []
    for i in range(n_clients):
        sources.append(Source((f"addr{randint(0, 1000)}", randint(0, 1000)),
                              f"Sänger {i}",
                              VOCAL_GROUPS[randrange(1, len(VOCAL_GROUPS))],
                              1.0))

    server = await asyncio.start_server(partial(control_server, clients=sources, volatility=volatility,
                                                presence=presence), port=port_number)

    addr = server.sockets[0].getsockname()
    logger.info(f'Serving on {addr}, volatility = {volatility}, presence = {presence}')

    async with server:
        await server.serve_forever()


def cli():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port-number', type=int, default=8858)
    parser.add_argument('-n', '--sources', type=int, default=10, help="The number of sources to fake")
    parser.add_argument('-v', '--volatility', type=float, default=0.1,
                        help="The probability of a source to change address")
    parser.add_argument('-r', '--presence', type=float, default=0.8,
                        help="The probability of a source to participate to a session")
    parsed_args = parse_args_and_set_logger(parser)

    asyncio.run(main(parsed_args.port_number, parsed_args.sources, parsed_args.volatility, parsed_args.presence))

    exit(0)


if __name__ == '__main__':
    cli()
