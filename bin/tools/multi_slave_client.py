import asyncio
import uvloop
from random import randint, random, randrange, seed

from onlinechoir.common.audio_codecs import StereoDecoder, AdpcmEncoder
from onlinechoir.common.auth import AuthenticationHeader
from onlinechoir.common.constants import FrameType, CHUNKS_PER_SECOND, VOCAL_GROUPS
from onlinechoir.common.log import logger, parse_args_and_set_logger


async def slave_client(server: str, port: int, num: int):
    group_idx = randrange(1, len(VOCAL_GROUPS))

    # connect
    reader, writer = await asyncio.open_connection(server, port)

    # auth
    writer.write(AuthenticationHeader(f'Client {num}', VOCAL_GROUPS[group_idx]).to_bytes())
    await writer.drain()

    # start
    await wait_for_session(reader, writer)


async def get_packet(reader) -> bytes:
    try:
        data = await reader.readexactly(StereoDecoder.PACKET_SIZE)
    except (ConnectionResetError, asyncio.IncompleteReadError):
        raise RuntimeError("Lost connection")
    return data


async def wait_for_session(reader, writer):
    latency = randint(5, 50)
    left = random() < 0.5
    logger.info(f"Wait for session with latency {latency} and side {'left' if left else 'right'}")

    loop = asyncio.get_event_loop()
    while True:
        data = await get_packet(reader)
        idx = StereoDecoder.peek_frame_index(data)
        if idx == FrameType.KEEPALIVE:
            logger.debug("Got keep-alive")
            writer.write(AdpcmEncoder.silence_frame(FrameType.KEEPALIVE))
            await writer.drain()
        elif idx == 0:
            logger.info("Session starts")
            start_time = loop.time() + 1 + latency / CHUNKS_PER_SECOND

            # check in
            writer.write(AdpcmEncoder.silence_frame(FrameType.CHECK_IN))
            await writer.drain()

            # schedule first packet
            loop.call_at(start_time, loop.create_task, send_chunk(reader, writer, data, start_time, left))
            return
        else:
            raise ValueError(f"Unexpected chunk {idx}")


async def send_chunk(reader, writer, data: bytes, start_time: float, left: bool):
    payload = data[:260] if left else data[:4] + data[260:516]
    writer.write(payload)
    data = await get_packet(reader)
    idx = StereoDecoder.peek_frame_index(data)
    logger.debug(f"got chunk {idx}")
    if idx < 0:
        await wait_for_session(reader, writer)
    else:
        loop = asyncio.get_event_loop()
        loop.call_at(start_time + idx / CHUNKS_PER_SECOND, loop.create_task,
                     send_chunk(reader, writer, data, start_time, left))


def cli():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('num_clients', type=int)
    parser.add_argument('-s', '--server', default='localhost', help="The address of the server")
    parser.add_argument('-p', '--port', type=int, default=8868,
                        help="The port number to be used on the server (slave port)")
    parser.add_argument('-r', '--random-seed', type=int)
    parsed_args = parse_args_and_set_logger(parser)

    if parsed_args.random_seed is not None:
        seed(parsed_args.random_seed)

    loop = uvloop.new_event_loop()
    asyncio.set_event_loop(loop)
    for i in range(parsed_args.num_clients):
        loop.create_task(slave_client(parsed_args.server, parsed_args.port, i))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    parser.exit(0)


if __name__ == '__main__':
    cli()
