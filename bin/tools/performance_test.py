import asyncio
import time

from onlinechoir.common.audio_codecs import AdpcmEncoder
from onlinechoir.common.constants import FrameType
from onlinechoir.common.log import parse_args_and_set_logger
from onlinechoir.server.audio_mixer import AudioMixer


async def main(n_chunks: int, n_clients: int):
    mixer = AudioMixer()
    for c in range(n_clients):
        mixer.register_source(str(c))

    # check in
    for c in range(n_clients):
        mixer.add_frame(str(c), AdpcmEncoder.silence_frame(FrameType.CHECK_IN))

    mixer.set_end_frame(n_chunks - 1)
    for i in range(n_chunks):
        for c in range(n_clients):
            mixer.add_frame(str(c), AdpcmEncoder.silence_frame(i))


def run(n_chunks: int, n_clients: int) -> float:
    t = time.time()
    asyncio.run(main(n_chunks, n_clients))
    return time.time() - t


def cli():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--clients", type=int, default=20)
    parser.add_argument("-d", "--chunks", type=int, default=10000)
    args = parse_args_and_set_logger(parser)

    print(run(args.chunks, args.clients))


if __name__ == '__main__':
    cli()
